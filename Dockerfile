FROM ubuntu:16.04
MAINTAINER sebastien.carrere@inra.fr

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get -q update && \
        DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends install \
        hardinfo wget patch vim gawk unzip apache2 zlib1g-dev libpng-dev libgd2-noxpm-dev build-essential && \
        apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get -q update && apt-get install -yq cron at libtree-simple-perl libbio-perl-perl ncbi-blast+ sqlite liberror-perl libswitch-perl libxml-simple-perl libjson-perl libapache-session-perl libnet-ldap-perl libapache-htpasswd-perl ssmtp fasttree mafft biosquid python2.7 python-scipy python-sklearn python-numpy python-fastcluster mcl libarray-utils-perl liburi-encode-perl embassy-phylip

WORKDIR /var/www/

ENV ROOTDIR=/var/www/family-companion

COPY . $ROOTDIR

WORKDIR $ROOTDIR

#Install 

RUN chown -R www-data.www-data $ROOTDIR

RUN a2enmod rewrite && \
	a2enmod cgi && \
	echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
	cp $ROOTDIR/site/cfg/site-docker.cfg $ROOTDIR/site/cfg/site.cfg && \
	cp $ROOTDIR/site/cfg/authentication-docker.cfg $ROOTDIR/site/cfg/authentication.cfg && \
	export USER=root && ./bin/int/admin_install.pl --docker && \
	cp $ROOTDIR/site/cfg/apache-docker.conf /etc/apache2/sites-available/000-default.conf

RUN cd $ROOTDIR/bin/ext/ && wget https://github.com/bbuchfink/diamond/archive/v0.9.14.tar.gz -O diamond.tgz && tar xfz diamond.tgz && cd diamond-0.9.14/ && ./build_simple.sh && cp diamond $ROOTDIR/bin/ext/diamond

WORKDIR	$ROOTDIR/site

CMD ["sh","-c","service apache2 start && /bin/bash &&  export USER=root"]
