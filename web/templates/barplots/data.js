var data = [
		{
			"path" : "yeast.iprscan.specific.singlecopy.Coils.xls",
			"title" : "Coils",
			"data" : [ {
				"count" : "3",
				"desc" : "",
				"name" : "Coil"
			} ]
		},
		{
			"title" : "GO",
			"data" : [ {
				"count" : "2",
				"desc" : "null",
				"name" : "GO:0003676"
			}, {
				"desc" : "null",
				"count" : "2",
				"name" : "GO:0015074"
			}, {
				"name" : "GO:0016459",
				"count" : "1",
				"desc" : "null"
			}, {
				"name" : "GO:0005524",
				"count" : "1",
				"desc" : "null"
			}, {
				"name" : "GO:0003774",
				"count" : "1",
				"desc" : "null"
			}, {
				"count" : "1",
				"desc" : "null",
				"name" : "GO:0005515"
			} ],
			"path" : "yeast.iprscan.specific.singlecopy.GO.xls"
		},
		{
			"title" : "Gene3D",
			"data" : [ {
				"name" : "G3DSA:3.30.420.10",
				"desc" : "",
				"count" : "2"
			}, {
				"name" : "G3DSA:4.10.270.10",
				"desc" : "",
				"count" : "1"
			}, {
				"desc" : "",
				"count" : "1",
				"name" : "G3DSA:3.40.50.300"
			} ],
			"path" : "yeast.iprscan.specific.singlecopy.Gene3D.xls"
		},
		{
			"path" : "yeast.iprscan.specific.singlecopy.INTERPRO.xls",
			"data" : [ {
				"name" : "IPR012337",
				"count" : "2",
				"desc" : "Ribonuclease H-like domain"
			}, {
				"desc" : "Integrase, catalytic core",
				"count" : "2",
				"name" : "IPR001584"
			}, {
				"name" : "IPR013103",
				"count" : "2",
				"desc" : "Reverse transcriptase, RNA-dependent DNA polymerase"
			}, {
				"name" : "IPR027401",
				"desc" : "Myosin-like IQ motif-containing domain",
				"count" : "1"
			}, {
				"name" : "IPR001609",
				"desc" : "Myosin head, motor domain",
				"count" : "1"
			}, {
				"count" : "1",
				"desc" : "IQ motif, EF-hand binding site",
				"name" : "IPR000048"
			}, {
				"count" : "1",
				"desc" : "P-loop containing nucleoside triphosphate hydrolase",
				"name" : "IPR027417"
			} ],
			"title" : "INTERPRO"
		},
		{
			"data" : [ {
				"name" : "00230+2.7.7.7",
				"count" : "2",
				"desc" : "null"
			}, {
				"count" : "2",
				"desc" : "null",
				"name" : "00240+2.7.7.7"
			} ],
			"title" : "KEGG",
			"path" : "yeast.iprscan.specific.singlecopy.KEGG.xls"
		},
		{
			"path" : "yeast.iprscan.specific.singlecopy.PANTHER.xls",
			"data" : [ {
				"name" : "PTHR11439",
				"count" : "2",
				"desc" : ""
			}, {
				"name" : "PTHR13140:SF364",
				"desc" : "",
				"count" : "1"
			}, {
				"name" : "PTHR11439:SF127",
				"count" : "1",
				"desc" : ""
			}, {
				"name" : "PTHR13140",
				"desc" : "",
				"count" : "1"
			}, {
				"desc" : "",
				"count" : "1",
				"name" : "PTHR11439:SF134"
			} ],
			"title" : "PANTHER"
		},
		{
			"title" : "PRINTS",
			"data" : [ {
				"name" : "PR00193",
				"count" : "1",
				"desc" : "Myosin heavy chain signature"
			} ],
			"path" : "yeast.iprscan.specific.singlecopy.PRINTS.xls"
		},
		{
			"path" : "yeast.iprscan.specific.singlecopy.Pfam.xls",
			"title" : "Pfam",
			"data" : [
					{
						"name" : "PF07727",
						"desc" : "Reverse transcriptase (RNA-dependent DNA polymerase)",
						"count" : "2"
					}, {
						"name" : "PF00665",
						"desc" : "Integrase core domain",
						"count" : "2"
					}, {
						"name" : "PF00063",
						"desc" : "Myosin head (motor domain)",
						"count" : "1"
					} ]
		},
		{
			"path" : "yeast.iprscan.specific.singlecopy.ProDom.xls",
			"data" : [ {
				"name" : "PD936484",
				"count" : "1",
				"desc" : "POLYPROTEIN GLYCOPROTEIN M G2 TRANSMEMBRANE NONSTRUCTURAL MEMBRANE CONTAINS: PRECURSOR SIGNAL"
			} ],
			"title" : "ProDom"
		}, {
			"path" : "yeast.iprscan.specific.singlecopy.ProSiteProfiles.xls",
			"data" : [ {
				"count" : "2",
				"desc" : "Integrase catalytic domain profile.",
				"name" : "PS50994"
			}, {
				"count" : "1",
				"desc" : "Myosin motor domain profile.",
				"name" : "PS51456"
			}, {
				"count" : "1",
				"desc" : "IQ motif profile.",
				"name" : "PS50096"
			} ],
			"title" : "ProSiteProfiles"
		}, {
			"title" : "SMART",
			"data" : [ {
				"desc" : "Myosin. Large ATPases.",
				"count" : "1",
				"name" : "SM00242"
			} ],
			"path" : "yeast.iprscan.specific.singlecopy.SMART.xls"
		}, {
			"path" : "yeast.iprscan.specific.singlecopy.SUPERFAMILY.xls",
			"title" : "SUPERFAMILY",
			"data" : [ {
				"desc" : "",
				"count" : "2",
				"name" : "SSF53098"
			}, {
				"desc" : "",
				"count" : "2",
				"name" : "SSF56672"
			}, {
				"desc" : "",
				"count" : "1",
				"name" : "SSF52540"
			}, {
				"name" : "SSF57997",
				"desc" : "",
				"count" : "1"
			} ]
		} ];