Ext.Loader.setConfig({
	enabled : true
});

// defaults attributes

// Enables text selection in grids
Ext.override(Ext.grid.View, {
	enableTextSelection : true
});

Ext.override(Ext.form.field.Base, {
	labelSeparator : ''
});

Ext
		.application({

			name : 'BP',

			appFolder : './js/int/src',

			requires : [ 'BP.globals.ConnectionData', 'Ext.window.MessageBox',
					'BP.view.Viewport', 'BP.view.form.V_NewAnalysis',
					'BP.view.form.V_CustomForm',
					'BP.view.form.V_CustomFieldSet',
					'BP.view.form.V_UploadFile', 'BP.view.form.V_NumberField',
					'BP.view.form.V_IprScanUpload',
					'BP.view.form.V_ProteomesFieldSet',
					'BP.view.form.V_ProteomeFieldSet',
					'BP.view.panel.V_MainPanel', 'BP.view.panel.V_Home',
					'BP.view.panel.V_ResultPanel',
					'BP.view.panel.V_ResultsPanel',
					'BP.view.panel.V_BannerPanel', 'BP.view.panel.V_HelpPanel',
					'BP.view.grid.V_ListAnalyses',
					'BP.view.grid.V_TreeGridResult',
					'BP.view.grid.V_KeyValueGrid',
					'BP.view.grid.V_GridProteomes', 'BP.globals.Utils',
					'BP.globals.Result', 'BP.globals.Ajax', 'BP.globals.Login',
					'BP.store.S_Analyses', 'BP.store.S_Result',
					'BP.store.S_KeyValue', 'BP.store.S_Proteome',
					'BP.store.S_GenericStore', 'BP.store.S_TreeResult',
					'BP.view.button.V_BuildQueryBuilder',
					'BP.view.button.V_QueryBuilder',
					"BP.view.button.V_RemoveQueryBuilder",
					"BP.view.querybuilder.V_QueryBuilderPanel",
					"BP.view.querybuilder.V_SearchForm",
					"BP.view.querybuilder.V_BlastForm",
					"BP.view.querybuilder.V_QueryResultGrid",
					"BP.view.querybuilder.V_QueryResultPanel",
					"BP.view.fasta.V_FastaWindow",
					"BP.view.fasta.V_FastaPanel",
					"BP.view.fasta.V_MultiFastaPanel",
					"BP.view.msa.V_MsaPanel",
					"BP.view.querybuilder.V_IncludeExclude",
					"BP.view.matrix.V_HeatMap", "BP.view.matrix.V_MatrixPanel",
					"BP.view.matrix.V_Card", "BP.view.matrix.V_CardWindow",
					"BP.store.S_KeyValue", "BP.view.log.V_WindowLog" ],

			views : [ 'Viewport' ],

			stores : [ 'S_Analyses' ],

			controllers : [ 'C_Login', 'C_Upload', 'C_EnableIprScan',
					'C_LaunchAnalysis', 'C_Proteome', 'C_ViewResult',
					'C_Reload', 'C_Reset', 'C_DeleteResult', 'C_Publish',
					'C_BuildQueryBuilder', 'C_QueryBuilder', 'C_Search',
					'C_QueryResultGrid', 'C_FastaPanel', 'C_Blast',
					'C_IncludeExclude', 'C_DeleteProteome' ],

			launch : function() {

				this.setDebugMode();

				/**
				 * Required to have tips
				 */
				Ext.tip.QuickTipManager.init();

				Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
					dismissDelay : 0
				});

				// test user connection
				var mail = "";

				BP.globals.Ajax
						.send({
							url : '../cgi/index.cgi',
							params : {
								authentication : 1
							},
							method : 'POST',
							serverFailureMessage : "Impossible to check login",
							jsonFailureMessage : "Check login: bad response from the server",
							successFunction : function(json) {
								BP.globals.ConnectionData.mail = json["email"];
							},
							callback : function() {

								// For test :
								// BP.globals.ConnectionData.mail =
								// "l.cottret@gmail.com";
								// loads personal resources
								// var store = Ext
								// .getStore("S_PersoApplication");
								//
								// var box = Ext.MessageBox.wait(
								// 'Processing',
								// 'Processing, please wait...');
								//
								// store
								// .load({
								// callback : function(
								// records, operation,
								// success) {
								//
								// box.hide();
								//
								// if (!success) {
								// Ext.MessageBox
								// .alert(
								// "Failed",
								// "Loading your resources has failed");
								//
								// }
								//
								// }
								//
								// });
								// End for test

								// creates multiemail validation

								var vp = Ext
										.create(
												"BP.view.Viewport",
												{
													listeners : {
														afterrender : function() {

															// Decode the url
															var param = Ext
																	.urlDecode(location.search
																			.substring(1));
															if (Ext
																	.isDefined(param["view"])
																	&& Ext
																			.isDefined(param["owner"])) {
																if (param['owner'] == "") {
																	param['owner'] = BP.globals.ConnectionData.mail;
																}

																BP.globals.ConnectionData.owner = param['owner'];

																BP.globals.Ajax
																		.send({
																			url : '../cgi/index.cgi',
																			params : {
																				"view" : param["view"],
																				"owner" : param["owner"]
																			},
																			waitMessage : 'Getting analysis...',

																			successValue : 1,

																			successFunction : function(
																					jsonAnalysis) {
																				if (Ext
																						.isDefined(jsonAnalysis.analyses)
																						&& jsonAnalysis.analyses.length > 0
																						&& Ext
																								.isDefined(jsonAnalysis.analyses[0].url)) {

																					BP.globals.Result
																							.displayResult(jsonAnalysis.analyses[0].url);
																				} else {
																					Ext.Msg
																							.alert(
																									"Failed",
																									"Error in the analysis format");
																				}
																			}

																		});

															} else if (Ext
																	.isDefined(param["analysisid"])
																	&& (Ext
																			.isDefined(param["proteinid"]) || Ext
																			.isDefined(param["groupid"]))) {

																var target = "";
																var query = "";
																var owner = undefined;
																if (Ext.isDefined(param["owner"])) {
																	owner = param["owner"];
																}
																
																if (Ext
																		.isDefined(param["proteinid"])) {
																	target = "proteins";
																	query = param["proteinid"];
																} else {
																	target = "groups";
																	query = param["groupid"]
																}

																
																console.log("target", target, "query", query);
																

																var resultPanel = Ext
																		.create(
																				"BP.view.panel.V_ResultPanel",
																				{
																					title : query
																				});

																var mainPanel = Ext.ComponentQuery
																		.query("mainPanel")[0];

																mainPanel
																		.add(resultPanel);
																
																mainPanel.setActiveItem(resultPanel);

																BP.globals.Result
																		.displayQueryResult(
																				resultPanel,
																				0,
																				param["analysisid"],
																				owner,
																				target,
																				"accession",
																				query,
																				50);

															}

														}
													}
												});

							}
						});

			},
			/**
			 * set debug mode if debug is in the url, DEBUG = true
			 */
			setDebugMode : function() {

				DEBUG = false;
				var param = Ext.urlDecode(location.search.substring(1));
				if (Ext.isDefined(param["debug"])) {
					DEBUG = true;
				}

			}

		});
