Ext.define('BP.model.M_KeyValue', {
	extend : 'Ext.data.Model',

	fields : [ {
		name : 'key',
		type : 'string'
	}, {
		name : 'value',
		type : 'string'
	}
	]
});