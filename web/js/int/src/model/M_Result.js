Ext.define('BP.model.M_Result', {
	extend : 'Ext.data.Model',

	fields : [ {
		name : 'name',
		type : 'string'
	},  {
		name : 'description',
		type : 'string'
	}, {
		name : 'message',
		type : 'string'
	}, {
		name : 'status',
		type : 'string'
	}, {
		name : 'link',
		type : 'string'
	} ]
});