Ext.define('BP.view.grid.V_GridProteomes', {
	extend : 'Ext.grid.Panel',
	requires : [],
	alias : 'widget.gridProteomes',
	resizable : false,
	closable : false,
	padding : 5,
	width : "100%",
	title : "Proteomes",
	autoScroll:false,
	
	collapsible : true,
	collapsed : true,
	
	margin:5,

	
	columns : [ {
		text : 'code',
		dataIndex : 'code',
		width:100,
		renderer : function(value) {
			return '<span style="font-weight:bold;">'+value+"</span>";
		}
	}, 
	{
		text : 'reference',
		width:100,
		dataIndex : 'reference',
	},
	
	{
		text : 'title',
		dataIndex : 'title',
		flex:2
	}

	]

});