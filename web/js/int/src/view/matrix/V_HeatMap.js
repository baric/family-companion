/**
 * 
 */
Ext
		.define(
				'BP.view.matrix.V_HeatMap',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.heatmap',
					requires : [],
					layout : 'fit',
					autoScroll : true,
					resizable : false,
					closable : true,

					url : "",

					/**
					 * 
					 */
					constructor : function(params) {

						this.url = params.url;

						this.callParent([ params ]);

					},

					/**
					 * 
					 */
					initComponent : function() {

						var panel = this;

						var posX = window.innerWidth - 260;

						var posY = 300;

						var win = Ext.create("BP.view.matrix.V_CardWindow", {
							visible : false,
							x : posX,
							y : posY,
							renderTo : panel.el
						});

						this.winCard = win;

						this.callParent(arguments);

						var heatmapId = this.id + "matrix";

						var container = Ext
								.create(
										"Ext.container.Container",
										{
											margin : 5,
											padding : 5,
											html : "<div style='margin:5;padding:5;height: 600px; min-width: 310px; max-width: 100%;' id='"
													+ heatmapId + "'></div>",
											name : "matrixZone",
											border : false
										});

						this.add(container);

						BP.globals.Ajax.send({
							url : this.url,
							waitMessage : "Build matrix...",
							successValue : "true",

							successFunction : function(json) {

								var a_links = json.metadata.links;
								
								if (!Ext.isDefined(a_links)) {
									Ext.Msg.alert("Failed",
											"No links to build the heatmap");
									return;
								}

								var rawDataLink = null;
								var inchlibLink = null;

								Ext.each(a_links, function(l) {
									if (l.rel == "rawdata"
											&& Ext.isDefined(l.href)) {
										rawDataLink = l.href;
									} else if (l.rel == "inchlib"
											&& Ext.isDefined(l.href)) {
										inchlibLink = l.href;
									}
								});


								if (rawDataLink != null) {
									 container.add({
										xtype : "button",
										tooltip : "Get raw data",
										iconCls : "icon-page_excel",
										handler : function() {
											window.open(rawDataLink, "_blank");
										}
									});
								}
								
								if (inchlibLink == null) {
									Ext.Msg.alert("Failed", "No inchlib link");
									return;
								}

								window.inchlib = new InCHlib({ // instantiate
									// InCHlib
									target : heatmapId, // ID of a target HTML
									// element
									metadata : true, // turn on the metadata
									column_metadata : false, // turn on the
									// column metadata
									max_height : 1200, // set maximum height of
									// visualization in
									// pixels
									width : 1000, // set width of
									// visualization in pixels
									heatmap_colors : "Greens", // set color
									// scale for
									// clustered
									// data
									metadata_colors : "Reds", // set color
									// scale for
									// metadata
									independent_columns : false,
									//~ draw row (object) ids next to the heatmap (only when there is enough space)
									draw_row_ids: true
								});

								
								
								inchlib.read_data_from_file(inchlibLink); // read
								// input
								// json
								// file
								var o_json = inchlib.json;
								if (o_json.compression.mode === 1)
								{
									inchlib.settings.metadata = false;
									var message = 'WARNING: ' + o_json.compression.input_lines + ' lines compressed into max ' + o_json.compression.threshold ;
									var component = container.add({
										xtype: 'box',
										html : '<div class="alert alert-warning">'+message+'</div>'
									});
								}
								

								inchlib.draw(); // draw cluster heatmap

								panel.inchlib = inchlib;

								panel.bind_dendrogram_events();

								
								panel.mouseOver = o_json.onmouseover;
								

							}
						});

					},

					/**
					 * 
					 */
					displayCard : function(ids, evt) {

						var o_info = this.mouseOver[ids];

						var a_data = [];

						for (key in o_info) {
							var value = o_info[key];

							a_data.push({
								"key" : key,
								"value" : BP.globals.Utils.formatValue(value)
							});

						}

						var store = Ext.create("BP.store.S_KeyValue");

						store.loadData(a_data);

						var grid = this.winCard.down("grid");

						grid.setStore(store);

						this.winCard.setVisible(true);

					},

					/**
					 * 
					 */
					hideCard : function() {
						this.winCard.hide();
					},

					/**
					 * 
					 */
					fixCard : function(inchlib) {
						inchlib.events.row_onmouseover = function() {
						};
						inchlib.events.heatmap_onmouseout = function() {
						};
					},

					/**
					 * 
					 */
					bind_dendrogram_events : function(inchlib) {

						var panel = this;

						var inchlib = panel.inchlib;
						var o_json = inchlib.json;
						inchlib.events.row_onmouseover = function(ids, evt) {
							if (o_json.compression.mode !== 1)	panel.displayCard(ids);
						};
						inchlib.events.heatmap_onmouseout = function(evt) {
							if (o_json.compression.mode !== 1)	panel.hideCard();
						};
						inchlib.events.row_onclick = function(ids, evt) {
							if (o_json.compression.mode !== 1)	panel.displayCard(ids);
							panel.fixCard(inchlib);
							inchlib.highlight_rows(ids);

						};
						inchlib.events.empty_space_onclick = function() {
							if (o_json.compression.mode !== 1)	panel.hideCard();
							inchlib.highlight_rows([]);
							panel.bind_dendrogram_events(inchlib);
						};
						
					}

				});
