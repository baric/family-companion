/**
 * 
 */
Ext
		.define(
				'BP.view.matrix.V_HeatMapHighchart',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.heatmapHC',
					requires : [],
					// layout : 'fit',
					autoScroll : true,
					resizable : false,
					closable : true,

					url : "",

					/**
					 * 
					 */
					constructor : function(params) {

						this.url = params.url;

						this.callParent([ params ]);

					},

					/**
					 * 
					 */
					initComponent : function() {

						var panel = this;


						this.callParent(arguments);

						var heatmapId = this.id + "matrix";

						var container = Ext
								.create(
										"Ext.container.Container",
										{
											margin : 5,
											padding : 5,
											html : "<div style='margin:5;padding:5;height: 600px; min-width: 310px; max-width: 100%;' id='"
													+ heatmapId + "'></div>",
											name : "matrixZone",
											border : false
										});

						this.add(container);

						BP.globals.Ajax
								.send({
									url : this.url,
									waitMessage : "Build matrix...",
									successValue : "true",

									successFunction : function(json) {

										var a_fields = json.fields;

										var a_matrix = json.matrix;

										// columns= proteomes
										var a_columns = [];

										Ext.each(a_fields, function(o_field) {

											if (o_field.name != "ROW") {
												a_columns.push(o_field.name);
											}
										});

										// rows = groups
										var a_rows = [];

										// contains triplets {x, y, value}
										var a_data = [];

										var x = 0;

										maxValue = 0;

										Ext
												.each(
														a_matrix,
														function(o_row) {

															a_rows
																	.push(o_row.ROW);

															for ( var proteome in o_row) {
																if (proteome != "ROW") {

																	var y = a_columns
																			.indexOf(proteome);

																	a_data
																			.push([
																					y,
																					x,
																					o_row[proteome] ]);

																	if (o_row[proteome] > maxValue) {
																		maxValue = o_row[proteome];
																	}

																}
															}

															x++;

														});

										var chart = new Highcharts.Chart(
												{

													chart : {
														type : 'heatmap',
														renderTo : heatmapId,
														marginTop : 40,
														marginBottom : 80,
														plotBorderWidth : 1
													},

													title : {
														text : 'Abundancy matrix'
													},

													xAxis : {
														categories : a_columns,
														labels : {
															enabled : (a_columns.length > 20) ? false
																	: true
														},
														tickLength : (a_columns.length > 20) ? 0
																: 16
													},

													yAxis : {
														categories : a_rows,
														title : null,
														labels : {
															enabled : (a_rows.length > 20) ? false
																	: true
														}
													},

													colorAxis : {
														min : 0,
														// stops : [
														// [ 0, '#3060cf' ],
														// [ 0.5,
														// '#fffbbc' ],
														// [ 0.9,
														// '#c4463a' ],
														// [ 1, '#c4463a' ] ],
														minColor : '#FFFFFF',
														maxColor : Highcharts
																.getOptions().colors[0],
														max : maxValue

													},

													legend : {
														align : 'right',
														layout : 'vertical',
														margin : 0,
														verticalAlign : 'top',
														y : 25,
														symbolHeight : 280
													},

													tooltip : {
														formatter : function() {

															var str = "";

															str = (a_columns.length < 500 && a_rows.length ) ? 'Abundancy of <b>'
																	+ this.series.yAxis.categories[this.point.y]
																	+ '</b> in <b>'
																	+ this.series.xAxis.categories[this.point.x]
																	+ '</b> : <b>'
																	+ this.point.value
																	+ '</b>'
																	: "Column too narrow to be picked up by the mouse";

															return str;
														}
													},

													series : [ {
														name : 'abundancy',
														borderWidth : 1, // No effect with the large heatmap plugin
														data : a_data,
														turboThreshold : Number.MAX_VALUE
													// #3404, remove after 4.0.5
													// release
													// dataLabels : {
													// enabled : true,
													// color : '#000000'
													// }
													} ]

												});

									}
								});

					}

				});
