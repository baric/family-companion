Ext.define('BP.view.matrix.V_Card', {
			extend : 'Ext.grid.Panel',
			alias : 'widget.cardGrid',

			viewConfig : {
				stripeRows : false
			},

			columns : [{
				text : 'Source',
				dataIndex : 'key',
				flex : 1
			}, {
				text : 'Value',
				dataIndex : 'value',
			}]

		});
