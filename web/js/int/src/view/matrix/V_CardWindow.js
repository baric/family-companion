/**
 * window containing a {@link Archive.view.card.V_CardGrid grid} 
 * displaying information about a selected submission
 */
Ext.define('BP.view.matrix.V_CardWindow', {
			extend : 'Ext.window.Window',
			alias : 'widget.cardWindow',

			title : "Metadata",

			height : 300,
			width : 250,
			autoRender : true,
			autoScroll : true,
			
			constrain : true,

			closable : true,
			closeAction : 'hide',

			layout : "fit",

			items : [{
						xtype : "cardGrid"
					}]

		});