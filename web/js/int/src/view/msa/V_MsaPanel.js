/**
 * Multi sequence alignment viewer
 */
Ext
    .define(
        'BP.view.msa.V_MsaPanel', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.msaPanel',
            requires: [],
            // layout : 'fit',
            autoScroll: true,
            resizable: false,
            closable: true,

            title: "MSA",

            layout: "fit",
            
            url : "",
            
            listeners: {
				afterrender: function() {
					this.buildMsa();
				}
            },

            /**
             * Constructor
             */
            buildMsa: function() {

            	var url = this.url;
            	
                var container = Ext
                    .create(
                        "Ext.container.Container", {
                            margin: 5,
                            padding: 5,
                            html: "<div style='margin:5;padding:5;' class='msa'></div>",
                            name: "msaZone",
                            border: false
                        });

                this.add(container);

                BP.globals.Ajax
                    .send({
                        url: url,
                        waitMessage: "Get alignment",
                        successValue: "true",

                        successFunction: function(json) {
                            var urlAlignment = null;
                            //~ SC
                            var urlTree = null;


                            var a_links = json.metadata.links;

                            Ext.each(a_links, function(l) {

                                if (l.rel == "rawdata") {
                                    urlAlignment = l.href;
                                } else if (l.rel == "tree") {
                                    urlTree = l.href;
                                }


                            });

                            if (urlAlignment == null) {
                                Ext.Msg
                                    .alert("Failed",
                                        "No file for this alignment");
                            } else {

                                var yourDiv = Ext.query('#' + container.getEl().id + " div.msa")[0];

                                var menuDiv = document.createElement("div");
                                var msaDiv = document.createElement("div");
                                var treeDiv = document.createElement("div");
				

                                treeDiv.className = "treeDiv";

                                treeDiv.innerHTML += "Tree computed with FastTree. Right-click on the tree to display options.<br />";
                                treeDiv.innerHTML += "<a href='"+urlTree+"'target=_blank>Newick file</a><br />";

                                yourDiv.appendChild(menuDiv);
                                yourDiv.appendChild(msaDiv);
                                yourDiv.appendChild(treeDiv);

                                //~ MSA
                                msa.io.fasta
                                    .read(
                                        urlAlignment,
                                        function(err, seqs) {

                                            var opts = {
                                                el: msaDiv,
                                                vis: {
                                                    conserv: false,
                                                    overviewbox: false,
                                                    labelId: false
                                                },
                                                zoomer: {
                                                    labelNameLength: 200
                                                },
                                                importUrl: urlAlignment,
                                                seqs: seqs,
                                                menu: "small",
                                                bootstrapMenu: true
                                            };

                                            var m = new msa(opts).render();

                                        });

                                //~ TREE

                                var xhr = new XMLHttpRequest();

                                xhr.onreadystatechange = function() {
                                    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                                        var newick = xhr.responseText;
                                        console.log("newick");
                                        console.log(newick);

                                        (function (Phylocanvas) {
                                            var tree = Phylocanvas.createTree(treeDiv, {"fillCanvas":true});
                                            tree.load(newick);

                                            var h = document.getElementsByClassName("phylocanvas-history")[0];

                                            h.remove();
                                          })(window.Phylocanvas);
                                        
                                        
                                    }
                                };

                                xhr.open("GET", urlTree, true);
                                xhr.send(null);


                            }

                        }
                    });

            }

        });
