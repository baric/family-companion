Ext.define('BP.view.querybuilder.V_SearchForm', {
	extend : 'Ext.form.Panel',

	alias : 'widget.searchForm',

	collapsible : true,

	margin : 5,
	padding : 5,

	layout : {
		type : "vbox"
	},

	defaults : {
		padding : 5,
		margin : 5,

	},

	items : [ {
		xtype : "fieldset",
		border : false,
		plugins : 'responsive',
		layout : {
			type : "hbox"
		},
		defaults : {
			padding : 5,
			margin : 5,

		},
		items : [ {
			xtype : "combobox",
			name : "target",
			width : 100,
			// fieldLabel : 'Proteins or Groups?',
			fieldLabel : '',
			store : new Ext.data.Store({
				fields : [ "name" ],
				data : [ {
					"name" : "proteins"
				}, {
					"name" : "groups"
				} ]
			}),
			value : "proteins",
			forceSelection : true,
			allowBlank : false,
			displayField : 'name',
			valueField : 'name'
		},

		{
			xtype : "combobox",
			width : 100,
			name : "query",
			// fieldLabel : 'Accession or Annotation?',
			fieldLabel : '',
			displayField : 'name',
			valueField : 'name',
			value : "accession",
			allowBlank : false,
			store : new Ext.data.Store({
				fields : [ "name" ],
				data : [ {
					"name" : "accession"
				}, {
					"name" : "annotation"
				} ]
			}),
			forceSelection : true

		}, {
			xtype : "textfield",
			width : 200,
			name : "queryText",
			fieldLabel : "",
			allowBlank : false,
			emptyText:"Use % as a wildcard"

		} ]
	},

	//	          
	// {
	// xtype : "fieldset",
	// layout : {
	// type : "hbox",
	// margin : 5,
	// padding : 5,
	// align : "stretch"
	// },
	// items : [ {
	// xtype : "textfield",
	// fieldLabel : "accession",
	// allowBlank : false
	//
	// }, {
	// xtype : "button",
	// text : "search",
	// action : "searchByAccession"
	// } ]
	// }, {
	// xtype : "fieldset",
	// layout : {
	// type : "hbox",
	// margin : 5,
	// padding : 5,
	// align : "stretch"
	// },
	// items : [ {
	// xtype : "textfield",
	// fieldLabel : "annotation",
	// emptyText: "kinase GO:0003677 PF13276"
	// }, {
	// xtype : "button",
	// text : "search",
	// action : "searchByAnnotation"
	// } ]
	// },
	{
		xtype : "numberfield",
		value : 50,
		allowBlank : false,
		fieldLabel : "Page size",
		name : "pageSize",
		minValue : 1,
		width : 200,
		maxWidth : 200,
		margin : 5,
		padding : 5,
		maxValue : 200
	}, {
		xtype : "button",
		text : "search",
		formBind : true,
		action : "search"
	}

	]
});
