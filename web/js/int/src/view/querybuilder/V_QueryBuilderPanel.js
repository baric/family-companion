Ext.define('BP.view.querybuilder.V_QueryBuilderPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.queryBuilderPanel',
	requires : [],
	// layout : 'fit',
	autoScroll : true,
	resizable : false,
	closable : true,

	title : "Query Builder",
	layout : {
		type : 'vbox',
		align : 'stretch',
		margin : 5,
		padding : 5,
	},

	initComponent : function() {

		var panel=this;
		
		panel.items = [ {
			xtype : "searchForm",
			title : "Search by key word"
		}, {
			xtype : "blastForm",
			title : "Blast"
		}, {
			xtype : "includeExcludeForm",
			title : "Include or exclude proteomes"
		}

		];

		
		var url = BP.globals.Utils.getHostUrl() + "/ws/" + panel.analysisName
				+ "/get/proteomes?field=code";

		
		
		panel.callParent();
		
		var a_data = [];

		BP.globals.Ajax.send({
			url : url,
			waitMessage : "Get proteomes",
			successValue : "true",

			successFunction : function(json) {

				var a_list = json.list;

				if (!Ext.isDefined(a_list)) {
					Ext.Msg.alert("Message",
							"Server error : no list in the json");
					return;
				}

				Ext.each(a_list, function(code) {
					a_data.push({
						"code" : code
					});
				});
				
				// Displays the proteome in the includeExcude form
				var centralGrid = panel.query("includeExcludeForm grid")[1];
				centralGrid.getStore().loadData(a_data);

			}
		});
		
		
		
	}

});
