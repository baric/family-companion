Ext.define('BP.view.querybuilder.V_QueryResultPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.queryResultPanel',
	requires : [],
	// layout : 'fit',
	autoScroll : true,
	resizable : false,
	closable : true,
	
	margin : 5,
	padding : 5,

	title : "",

	items : [],

	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	
	listeners : {
		'afterrender' : function(panel) {
			
			this.up("panel").scrollBy(0, -100);
			
			panel.focus();

		}
	}
	

});