/**
 * Multi Fasta panel
 */
Ext
		.define(
				'BP.view.fasta.V_MultiFastaPanel',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.multiFastaPanel',
					requires : [],
					// layout : 'fit',
					autoScroll : true,
					resizable : true,

					collapsible : false,

					header : false,

					title : "fasta",

					layout : {
						type : "vbox",
						align : "stretch"
					},

					/**
					 * Constructor
					 * 
					 * Must have a json key
					 * 
					 */
					constructor : function(params) {

						if (!Ext.isDefined(params.json)) {
							console
									.log("lacks the json parameters in the constructor of the fasta window");
						}
						var o_json = params.json;
						
						var rawDataLink = null;

						var a_links = o_json.metadata.links;

						Ext.each(a_links, function(l) {

							if (l.rel == "rawdata") {
								rawDataLink = l.href;
							}

						});

						if (rawDataLink != null && Ext.isDefined(rawDataLink)) {

							params.dockedItems = [

							{
								dock : 'top',
								xtype : 'toolbar',

								items : [ {
									xtype : "button",
									tooltip : "Load fasta file",
									iconCls : "icon-bullet_disk",
									handler : function() {
										window.open(rawDataLink, "_blank");
									}
								} ]

							} ];
						}

						var sequences = o_json.sequences;

						params.items = [];

						Ext.each(sequences, function(s) {

							var fastaPanel = Ext.create(
									"BP.view.fasta.V_FastaPanel", {
										json : s
									});

							params.items.push(fastaPanel);

						});

						this.callParent([ params ]);

					}

				});
