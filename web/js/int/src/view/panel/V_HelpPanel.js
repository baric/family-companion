Ext.define('BP.view.panel.V_HelpPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.helpPanel',
	
	autoScroll : true,
	resizable : true,
	
	title : "Help",
	
	padding : 10,
	
	loader: {
	    url: 'help.html',
	    autoLoad: true
	}
});
