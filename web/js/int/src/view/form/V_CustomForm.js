Ext.define('BP.view.form.V_CustomForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.customForm',
    initComponent: function() {
        this.on('beforeadd', function(me, field){
          if (!field.allowBlank && field.xtype!="checkboxfield" && field.xtype!="displayfield")
            field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });
        this.callParent(arguments);
      }
    
});