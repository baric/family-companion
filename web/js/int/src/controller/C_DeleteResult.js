/**
 * Controls the action column of the grid that removes a result
 */
Ext.define('BP.controller.C_DeleteResult', {
	extend : 'Ext.app.Controller',

	requires : [ 'BP.view.panel.V_ResultsPanel' ],

	init : function() {
		this.control({
			'listAnalyses' : {
				deleteresult : this.deleteResult
			},
		});

	},

	/**
	 * 
	 */
	deleteResult : function(record, rowIndex) {

		var data = record.store.getAt(rowIndex).data;

		var base = BP.globals.Utils.basename(data.url);

		Ext.Ajax.request({
			url : '../cgi/index.cgi',
			params : {
				"delete" : base
			},
			waitMsg : 'Deleting...',
			success : function(fp, o) {
				Ext.Msg.alert("Success", "Analysis successfully deleted");

				Ext.ComponentQuery.query("listAnalyses")[0].down(
						'button[action="reload"]').fireEvent("click");

			},
			failure : function(fp, o) {
				Ext.Msg.alert("Failure", "Failure while deleting analysis");
			}

		});

	}
});