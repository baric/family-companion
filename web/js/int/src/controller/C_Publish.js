Ext.define('BP.controller.C_Publish', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'listAnalyses' : {
				publish : this.publish
			}
		});

	},

	publish : function(record, rowIndex) {

		var data = record.store.getAt(rowIndex).data;

		var base = BP.globals.Utils.basename(data.url);

		BP.globals.Ajax.send({

			url : '../cgi/index.cgi',
			params : {
				"publish" : base
			},
			waitMessage : 'Publishing...',
			successValue : "1",

			successFunction : function(json) {

				Ext.Msg.alert("Success",
						"Analysis public status successfully updated.<br />"
								+ json.message);
			}

		});

	}
});