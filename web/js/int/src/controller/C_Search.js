Ext.define('BP.controller.C_Search', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'button[action=search]' : {
				click : this.search
			}
		});

	},

	search : function(button) {

		var query = "accession";
		if (button.action == "searchByAnnotation") {
			query = "annotation";
		}

		var queryBuilderPanel = button.up("queryBuilderPanel");

		var analysisName = queryBuilderPanel.analysisName;

		var searchPanel = button.up("searchForm");

		var o_values = searchPanel.getValues();

		var target = o_values.target;

		var query = o_values.query;

		var owner = undefined;
		if (o_values.owner)
		{
			owner = o_values.owner;
		}

		var acc = encodeURIComponent(o_values.queryText);

		var pageSize = o_values.pageSize;
		
		var idx = queryBuilderPanel.items.indexOf(searchPanel) + 1;
		
		BP.globals.Result.displayQueryResult(queryBuilderPanel, idx, analysisName, owner, target, query, acc, pageSize );

	}

});
