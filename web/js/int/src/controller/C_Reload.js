Ext.define('BP.controller.C_Reload', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'button[action=reload]' : {
				click : this.reload
			},
		});

	},

	/**
	 * 
	 */
	reload : function(button) {

		var a_log_windows = Ext.ComponentQuery.query("windowLog");

		Ext.each(a_log_windows, function(win) {
			win.close();
		});

		Ext.getStore("S_Analyses").reload();

	}
});