Ext.define('BP.controller.C_LaunchAnalysis', {
	extend : 'Ext.app.Controller',

	requires : [ 'BP.globals.ConnectionData' ],

	init : function() {
		this.control({
			'button[action=launchAnalysis]' : {
				click : this.launchAnalysis
			},
		});

	},

	launchAnalysis : function(button) {

		var panel = button.up("form");
		
		var form = panel.getForm();
		
		var codes = [];

		var ERROR_STRING = "";
		form.getFields().each(
				function(item) {

					if (!item.isValid()) {
						ERROR_STRING = ERROR_STRING + "The field '"
								+ item.fieldLabel + "' is invalid, reasons: "
								+ item.getErrors() + "<br />";
					}
				});
		if (ERROR_STRING.length > 0) {
			Ext.MessageBox.alert('Error', ERROR_STRING);
		}

		if (ERROR_STRING.length == 0 && form.isValid()) {

			var values = form.getValues();

			
			if (typeof values.code == "string") {
				values.code = [ values.code ];
				
				values.pathProteome = [ values.pathProteome ];
				values.proteomeDescription = [ values.proteomeDescription ];
				values.isReference = [ values.isReference ];
				if (typeof values.pathIprScan !== 'undefined') {
					values.pathIprScan = [ values.pathIprScan ];
				}
			}

			if (typeof values.pathIprScan === 'undefined') {
				values.pathIprScan = [];

				for (var i = 0; i < values.code.length; i++) {
					values.pathIprScan[i] = "";
				}
			}

			//~ console.log(values);

			var json = {};

			json.contact = BP.globals.ConnectionData.mail;
			json.title = values.analysis_title;
			var d = new Date();
			json.date = Ext.Date.format(d, 'Ymd H:i:s');
			json.description = values.analysis_description;
			json.homologygroups = {
				pmatch_cutoff : values.pMatchCutoff,
				pi_cutoff : values.piCutoff,
				pv_cutoff : values.pvCutoff,
				inflation : values.inflation,
				outfile : values.pathHomologyGroups
			};

			json.proteomes = {};

			for (var i = 0; i < values.code.length; i++) {
				
				
				if(Ext.Array.contains(codes, values.code[i]))
				{
					Ext.MessageBox.alert('Error', "Proteome codes must be unique : "+values.code[i]+" is duplicated");
					return;
				}
				
				Ext.Array.push(codes, values.code[i]);

				var proteome = {};
				proteome.reference = values.isReference[i];
				proteome.fasta = values.pathProteome[i];
				proteome.title = values.proteomeDescription[i];
				proteome.iprscan = values.pathIprScan[i];

				json.proteomes[values.code[i]] = proteome;

			}

			var json_str = Ext.JSON.encode(json);

			var win_wait = Ext.create("Ext.window.MessageBox");

			var win_alert = Ext.create("Ext.window.MessageBox");

			win_wait.wait("Launching analysis...", "Please Wait");

			Ext.Ajax.request({
				url : '../cgi/index.cgi',
				scope : this,
				method : 'POST',
				params : {
					json : json_str,
					__wb_function : "LaunchAnalysis"
				},
				failure : function(response, opts) {
					win_alert.alert("Error", "Error during analysis (1)");
					win_wait.close();
				},
				success : function(response, opts) {

					var json = null;

					try {

						json = Ext.decode(response.responseText);
						
						if(! json["success"]) {
							
							win_alert.alert("Error", json.message);
							win_wait.close();
						}
						else {
							win_alert.alert("Success", json.message +"<br />Please regularly press the reload button in the panel 'List Of Analyses' to follow the progress of your analyses");
						}
						
						
					} catch (e) {
						win_alert.alert("Error", "Error during analysis (2)");
						win_wait.close();
					}

					var mainPanel = Ext.ComponentQuery.query("mainPanel")[0];
					var resultsPanel = Ext.ComponentQuery.query("listAnalyses")[0];
					console.log(resultsPanel);
					
					mainPanel.setActiveItem(resultsPanel);
					
					Ext.getStore("S_Analyses").reload();
					

				},
				callback : function() {
					win_wait.close();
				}

			});

		}

	}
});
