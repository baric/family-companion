/**
 * 
 */
Ext.define('BP.controller.C_RemoveQueryBuilder', {
	extend : 'Ext.app.Controller',

	/**
	 * @private init function
	 */
	init : function() {
		this.control({
			'removeQueryBuilderButton' : {
				click : this.remove
			},
		});

	},

	/**
	 * Remove the query builder of a button
	 */
	remove : function(button) {

		var analysisName = button.analysisName;

		var url = BP.globals.Utils.getHostUrl() + "/ws/" + analysisName
				+ "/status/remove";

		BP.globals.Ajax.send({
			url : url,
			waitMessage : "",
			successValue : "true",
			successFunction : function(json) {
				console.log(json);
				
				Ext.Msg.alert("Message", json["message"]);
				
				button.disable();
				
				var queryBuilderButton = button.previousSibling("buildQueryBuilderButton");
				
				queryBuilderButton.disable();
				
				
			}
		});

	}
});
