Ext.define('BP.controller.C_Login', {
	extend : 'Ext.app.Controller',

	init : function() {
		this.control({
			'button[action=login]' : {
				click : this.login
			},
		});

	},

	login : function(button) {
		window.onbeforeunload = null;
		window.open("../cgi/index.cgi?login=1", "_self","width=800,height=600,left=20,top=200");

	}
});
