#!/bin/bash

sudo bash
git clone https://framagit.org/BBRIC/family-companion.git
cd family-companion
export FC=$PWD
$FC/build.sh
$FC/configure.sh --local
$FC/run.sh --local
