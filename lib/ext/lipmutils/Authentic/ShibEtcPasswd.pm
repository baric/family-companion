#
#   emmanuel.courcelle@toulouse.inra.fr
#

=pod

=head1 NAME

Authentic::ShibEtcPasswd - An object to manage authentification from Shiboboleth AND /etc/passwd like file for the roles
                           It is VERY similar to Authentic::ShibEtcPasswd, except that the passwords are NOT managed by this module
                           The login is read from the env variable REMOTE_USER if it exists, or HTTP_REMOTE_USER

=head1 SYNOPSIS

    my $o_authentic = New Authentic::ShibEtcPasswd (
                          file      => 'some_file.prv'
 			        shib_login_url  => The Shibboleth login url
			        shib_logout_url => The Shibboleth logout url
			        target_url      => The url to redirect to after login
			        -return_url     => the url to redirect to after logout (if '', use target_url)
			        -provider_id    => the url of the identity provider (if not specieif, use the default wayf service)
			       -workspace_parent=> path to the parent directory of the workspace (see Authentic)			                          
			        -cookie_name    => cookie name (def &COOKIE_NAME)
			        -cookie_expires => Expiration date of the cookie (def +24h)
			        -o_logger       => A logger object
                    );
    
    $authentic tries to authenticate from the session, then from Shibboleth (looking from the env variables),
    at last redirecting to the provider url, if this is given (-provider_id)
    If the authentication is OK, it tries to find a valid role in the Passwd file
    If none is found, we are considered as NOT AUTHENTICATED
    
=head1 DESCRIPTION

    The .priv file has the following format:
    emcourcelle@inra.fr:::1::/www/LeARN_dev/web/tmp:
    
    The fields are separated by ':'
    field 1 (cnoirot) => The user name
    field 2           => not used
    field 3           => not used
    field 4 (1)       => A privilege OR a list of roles, may be numbers (privileges) or ;-separated list of strings. The following are all correct:
                           1
                           2                  => numbers, you may manage privileges by levels
                           user,projet1,demo  => indicating a role (user) in an application context (projet1,demo)
                                                 You may have any number of fields, the separator is the coma (,)              
                                                 strings, you must test for equality or for regex match:
                                                 if ($o_authentic->HasRole('user'))                 ==> return &TRUE if you are 'user' in any project
                                                 if ($o_authentic->HasRole('user','project4,demo')) ==> return &TRUE if you are 'user' in 'project&,demo'
                           user,project1,demo;user,project2,demo => list of roles, the separator is the ;
                                                 CHARACTERS ALLOWED= [a-zA-Z0-9]
                           NOTE - Privilege = 0 IS RESERVED, means: NOT AUTHENTICATED
    field 5           => Not used
    field 6 (/www/...)=> A user's "home directory"
    field 7           => not used
    
    It is the SAME format as the file used by EtcPasswd, except for the fields 2 and 5, which are NOT used.
    Thus it is possible to use a single file for both objects

=cut

package Authentic::ShibEtcPasswd;
use base qw( Authentic );

use strict;
use warnings;

use IO::File;
use Digest::MD5 qw(md5 md5_hex md5_base64);
#use Data::Dumper;
use Error qw( :try );
use CGI qw/:standard/;

use LipmError::IOException;
use General;

# The providerIds which will be proposed by the integrated wayf
# They will be proposed in the order of @A_PROVIDER_NAMES, so that you may control the display order
# The Other option will redirect to the standard wayf, with all members - It is recognized by the 'wayf' reserved word
our @A_PROVIDERS = qw( INRA CNRS CIRAD Other... );
our %H_PROVIDERS = (INRA       => 'https://idp.inra.fr/idp/shibboleth',
					CNRS       => 'urn:mace:cnrs.fr:janus.dsi.cnrs.fr',
					CIRAD      => 'https://idp.cirad.fr/idp/shibboleth',
					'Other...' => 'wayf' );

# The list of organizations we accept - If the organization is NOT in this list, authentication is refused !
# Organization means: "the right part of the eppn" ex. inra.fr, cnrs.fr, etc.
#
# NOTE - This array is the default value of the constructor parameter -authorized_orga
#        If the list is empty this test is NOT done
# 
our @A_AUTHORIZED_ORGA = qw( inra.fr cnrs.fr cirad.fr ird.fr );

=head2 function _Init

 Title		  : _Init (the constructor)
 Usage		  : $o_auth= New EtcPasswd ( %h_args );
 Function	  : constructor
 Args		  : %h_args The arguments (the leading - means optional argument):
			       file            : path to the passwd file
			       shib_login_url  : The Shibboleth login url
			       shib_logout_url : The Shibboleth logout url
			       target_url      : The url to redirect to after login
			       -return_url     : the url to redirect to after logout (if '', use target_url)
			       -provider_name  : the name of the identity provider (if not specified, use the default wayf service)
			                         It must be one of the keys of %H_PROVIDERS, else it is ignored
			        -ra_authorized_orga=> The list of restricted organizations, default \@A_AUTHORIZED_ORGA
			                              If you pass [], no eppn are restricted 
			       -workspace_parent: path to the parent directory of the workspace (see Authentic)			                          
			       -cookie_name    : cookie name (def &COOKIE_NAME)
			       -cookie_expires : Expiration date of the cookie (def +24h)
			       -o_logger       : A logger object

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
        file            => undef,
        shib_login_url  => '',
        shib_logout_url => '',
        target_url      => '',
        -return_url     => '',
        -provider_name  => undef,
        -ra_authorized_orga=> \@A_AUTHORIZED_ORGA,
        #-workspace_parent
        #-cookie_name    =>
        #-cookie_expires => 
        #-o_logger       => undef,
        @_);
     
    # Check required parameters
    foreach my $prm ( qw( file ) )
    {   
		die "Parameters missing" unless defined $h_args{ $prm };
	}
	
	# Init the base class
	$self->SUPER::_Init( %h_args);
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( file shib_login_url shib_logout_url target_url -return_url -provider_name -ra_authorized_orga ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	if ( $self -> _Get ( '__provider_name' ) ne '' && exists $Authentic::ShibEtcPasswd::H_PROVIDERS{ $self -> _Get ( '__provider_name' ) } )
	{
		my $provider_name = $self -> _Get ( '__provider_name' ) ;
		my $provider_id   = $Authentic::ShibEtcPasswd::H_PROVIDERS{ $provider_name };
		$self -> _Set ( '__provider_id',$provider_id );
	}
	
	# Try several authentication methods, the first working is OK
	$self->_AuthenticateFromSession() or $self->__AuthenticateFromShib() or $self->__AuthenticateFromProviderId($self->_Get('__provider_id'));

	return $self;
}

=head2 Function GetEppn
 Title    : GetEppn
 Usage    : my $eppn = $self -> GetEppn();
 Function : Returns the 'domain' part of the eppn, identifying the organization (the epst, unversite, etc)
 Return   : The eppn if authentified, undef if not authenticated
 Error    : Throw an error if we are authenticated but the EPPN is empty - SHOULD NOT HAPPEN, see __AuthenticateFromShib

=cut

sub GetEppn
{
	my $self = shift;
	my $eppn = undef;
	if ( $self -> IsAuthenticated() == &TRUE )
	{
		$eppn = $ENV { eppn } if ( defined $ENV { eppn } );
		$eppn = $ENV { HTTP_EPPN } if ( defined $ENV { HTTP_EPPN } );
		if ( defined ( $eppn) && $eppn eq '' )
		{
			throw Error::Simple ( "ERROR - eppn should NOT be blank !" );
		}
	}
	return $eppn;
}

=head2 Function GetOrganization
 Title    : GetOrganization
 Usage    : if ( $o_authentic -> GetOrganization() eq 'inra.fr' ) ...
 Function : Returns the 'domain' part of the eppn, identifying the organization (the epst, unversite, etc)
 Args     : $eppn The eppn: if undef, a call to GetEppn is performed (MUST BE AUTHENTICATED THUS)
 Return   : The organization if found, or undef if not authenticated
 Error    : Throw an error if we are authenticated but the EPPN is empty - SHOULD NOT HAPPEN, see __AuthenticateFromShib

=cut
sub GetOrganization
{
	my $self = shift;
	my ($organization) = @_;
	$organization = $self -> GetEppn() if (!defined $organization);
	
	if ( defined ($organization) && $organization ne '' )
	{
		$organization =~ s/^.+@//;   # Keep only the organization part of the eppn
	}
	return $organization;
}

=head2 Function __AuthenticateFromShib

 Title	  : __AuthenticateFromLogin
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromShib() == &TRUE);
 Prerequisite : Shibboleth is configured to authenticate the application
 Function : Look for REMOTE_USER, and if not found for HTTP_REMOTE_USER
            Start a new session and set the cookie
            If OK, set a lot of attributes
                
 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub __AuthenticateFromShib
{
	my $self = shift;
	
	my ($login,$email,$common_name,$identity_provider,$eppn) = ("","","","","");
	if ( defined $ENV { REMOTE_USER } )
	{
		$login       = $ENV { REMOTE_USER };
		#$login       = $ENV { mail };
		$email       = $ENV { mail };
		$common_name = $ENV { cn };
		$identity_provider = $ENV { Shib_Identity_Provider };
		$eppn        = $ENV { eppn } if (exists $ENV{ eppn });

	}
	elsif ( defined $ENV { HTTP_REMOTE_USER } )
	{
		$login       = $ENV { HTTP_REMOTE_USER };
		#$login       = $ENV { HTTP_MAIL };
		$email       = $ENV { HTTP_MAIL };
		$common_name = $ENV { HTTP_CN };
		$identity_provider = $ENV { HTTP_SHIB_IDENTITY_PROVIDER };
		$eppn        = $ENV { HTTP_EPPN } if (exists $ENV{ HTTP_EPPN });

	}
    
	# login empty or eppn empty or not authorized ==> not authenticated
	if (!defined($login) || $login eq '')
	{
		$self->_Trace("__AuthenticateFromShib login EMPTY => FALSE\n");
		return &FALSE;
	}
	if ( !defined ($eppn) || $eppn eq '' )
	{
		$self->_Trace("__AuthenticateFromShib eppn EMPTY => FALSE\n");
		return &FALSE;
	}
	else
	{
		my $ra_authorized_orga = $self -> _Get ( '__ra_authorized_orga' );
		my $orga = $self -> GetOrganization($eppn);
		if ( @$ra_authorized_orga > 0 )
		{
			my $found_orga = &FALSE;
			foreach my $o ( @$ra_authorized_orga )
			{
				if ( $o eq $orga )
				{
					$found_orga = &TRUE;
					last;
				}
			}
			if ( $found_orga == &FALSE )
			{
				$self->_Trace("__AuthenticateFromShib orga $orga NOT AUTHORIZED => FALSE\n");
				return &FALSE;
			}
		}
	}

	# Read the roles from file
	my $file_etc    = $self->_Get('__file');
	my $fh_file_etc = new IO::File($file_etc) or throw LipmError::IOException($file_etc, 'f');
	
	my $fnd_flg = &FALSE;
	my ($role, $workspace) = ("","");
    while (my $line = <$fh_file_etc>)
    {
        chomp($line);

        # blank lines are ignored
        next if ($line eq "");

        my @a_user = split(/:/, $line);

        if (uc($a_user[0]) eq uc($login))
	    {
	        $fnd_flg   = &TRUE;
			$role      = $a_user[3];
			$workspace = $a_user[5];
            last;
		}
	};

	# If found, Start a session, and set the attributes if OK
	my @a_roles = split /;/,$role;
	if ( $fnd_flg == &TRUE && $self->_NewSession($login,$common_name,\@a_roles,$workspace,$email,$identity_provider) == &TRUE )
	{
		$self->_Trace("__AuthenticateFromShib: user $login authenticated\n");	
		$self->_Set('_common_name',$common_name);
		$self->_Set('_identity_provider',$identity_provider);
	    $self->_Set('_ra_roles',\@a_roles);
		$self->_Set('_workspace',$workspace);
		$self->_Set('_login',$login);
        $self->_Set('_email',$email);
        $self->_Set('_Authenticated',&TRUE);
        return &TRUE;
	}
	else
	{
		$self->_Trace("__AuthenticateFromShib: user $login not NOT authenticated\n");
		$self->_Set('_common_name','');
		$self->_Set('_identity_provider','');
		$self->_Set('_ra_roles',[ 0 ]); 
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
		return &FALSE;
	}
}

=head2 Function __AuthenticateFromProviderId

 Title	  : __AuthenticateFromLogin
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromProviderId( $provider_url ) == &TRUE);
 Prerequisite : Shibboleth is configured to authenticate the application
 Function : Build a redirect url and redirect the browser to it
 Args     : $provider_url (optional): The url of the wanted provider if authentication wanted
            If $provider_url='wayf', the default wayf is specified
                
 Return	  : 1  if redirected
            0 if no provider_id specified, thus no authentification wanted
 
=cut
sub __AuthenticateFromProviderId
{
	my $self = shift;	
	my ($provider_url) = @_;
	$provider_url ="" unless (defined $provider_url);
	if ( $provider_url eq "" )
	{
		$self->_Trace("Starting __AuthenticateFromProviderId: NO AUTHENTICATION WANTED\n");
		return &FALSE;
	}
	else
	{
		# check the validity of some private variables (better doing now than in _Init, for performance reasons)
		foreach my $prm ( qw( __shib_login_url __target_url) )
		{
			if ( $self -> _Get($prm) eq '' )
			{
				throw Error::Simple ( "ERROR - $prm should NOT be blank !" );
			}
		}
	
		if ( $self->IsAuthenticated() == &FALSE )
		{
			my $shib_url     = $self -> _Get ( '__shib_login_url' );
			my $target_url   = $self -> _Get ( '__target_url' );
			my $redirect_url = $shib_url . '?target=' . $target_url;
			if ( $provider_url ne 'wayf')
			{
				$redirect_url .= '&providerId=' . $provider_url;
			}
	
			$self->_Trace("Starting __AuthenticateFromProviderId: redirecting to $redirect_url\n");
			print redirect($redirect_url);
		}
		return &TRUE;
	}
}

=head2 Function GetLoginForm

	Title    : GetLoginForm
	Usage    : print $o_authentic -> GetLoginForm($o_webbuilder);
	           Should be called from a callback for WebBuilder's Realize function
	Function : Build and return an integrated Wayf form, then redirect to the Shibboleth's lazy session url
	Args     : $o_webbuilder The calling object
	Globals  : %H_PROVIDERS, @A_PROVIDERS (the list of proposed identity providers)

=cut	
sub GetLoginForm
{
	my $self = shift;
    my ($o_webbuilder) = @_;
    
    my $rvl = "";
    if ($self->IsAuthenticated() == &FALSE)
    {
		$rvl .= '<h1>Login using the "Fédération d\'Identités Education-Recherche"</h1>';
		my $str_input = '<p><select name="provider_name">';
		foreach my $prov ( @Authentic::ShibEtcPasswd::A_PROVIDERS )
		{
			$str_input .= "<option value=\"$prov\" >$prov</option>";
		}
		$str_input .= '</select></p>';
		$str_input .= '<p><input type="submit" /></p>';
        $rvl .= $o_webbuilder->GetForm("", $str_input);
    }
    else
    {
        $rvl .= "You are now connected";
    }
    return $rvl;
}

=head2 Procedure Logout

 Title	  : Logout
 Usage    : $o_auth->Logout($url);
 Procedure: Call the superclass's Logout, terminating the local session
            Then redirect to the Shibboleth's logout url
 Args     : $shib_url The shibboleth logout url
			$return_url The url to be passed to the return= parameter
 
=cut
sub Logout
{
	my $self = shift;
	
	if ( $self->IsAuthenticated() == &TRUE )
	{
		# check the validity of some private variables (better doing now than in _Init, for performance reasons)
		foreach my $prm ( qw( __shib_logout_url __target_url) )
		{
			if ( $self -> _Get($prm) eq '' )
			{
				throw Error::Simple ( "ERROR - $prm should NOT be blank !" );
			}
		}
		my $return_url = ($self->_Get('__return_url') ne '') ? $self->_Get('__return_url') : $self->_Get('__target_url');
		my $shib_url   = $self->_Get('__shib_logout_url');

		$self->SUPER::Logout();
		$shib_url .= "?return=$return_url";
		print redirect($shib_url);
	}
}

1;
