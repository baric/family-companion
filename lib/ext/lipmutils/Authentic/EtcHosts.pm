#
# 	Sebastien.Carerre@toulouse.inra.fr
# 	Celine.noirot@toulouse.inra.fr
#   emmanuel.courcelle@toulouse.inra.fr
#	Created: March 08, 2006
#   Updated: Feb 08
#

=pod

=head1 NAME

Authentic::EtcHosts - An object to manage authentification from an /etc/hosts like file

=head1 SYNOPSIS

    my $o_authentic = New Authentic::EtcHosts (
                          file      => 'some_file.prv',
                          -ip    => $ip_adress
                      );

	my $o_authentic = New Authentic::EtcHosts (
                          file      => 'some_file.prv',
                          -hostname    => $hostname
                      );

    
    If -ip or -hostname is provided, $o_authentic checks them against the values found in the .priv file
    It then creates a new session (using Apache::Session::File) and writes a cookie
    If they are NOT provided, $o_authentic tries to access the cookie to find an already opened session
    
=head1 DESCRIPTION

    The .priv file has the following format:
	# This is an example of the hosts file
	127.0.0.1  localhost loopback
	::1        localhost
    
    The fields are separated by a white space or tab
    field 1 => IP address 
    field 2 and over => hostname and aliases
    
=cut

package Authentic::EtcHosts;
use base qw( Authentic );

use strict;
use warnings;

use IO::File;

#use Data::Dumper;
use Error qw( :try );

use LipmError::IOException;
use General;


=head2 function _Init

 Title		  : _Init (the constructor)
 Usage		  : $o_auth= New EtcPasswd ( %h_args );
 Function	  : constructor
 Args		  : %h_args The arguments (the leading - means optional argument):
                   -ip             : IP adress
			       -hostname       : host name or aliaspassword
			       file            : path to the hosts file
			       -workspace_parent: path to the parent directory of the workspace (see Authentic)			                          
			       -cookie_name    : cookie name (def &COOKIE_NAME)
			       -cookie_expires : Expiration date of the cookie (def +24h)
			       -o_logger       : A logger object

=cut

sub _Init
{
	my $self = shift;
	my %h_args = (
        -ip    => undef,
        -hostname => undef,
        file      => undef,
        #-workspace_parent
        #-cookie_name    =>
        #-cookie_expires => 
        #-o_logger       => undef,
        @_);
     
    # Check required parameters
    foreach my $prm ( qw( file ) )
    {   
		die "Parameters missing" unless defined $h_args{ $prm };
	}
	
	# Init the base class
	$self->SUPER::_Init( %h_args);
	
	# Init private attributes (removing the '-' sign)
    foreach my $prm ( qw( file ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '__' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Init protected attributes (removing the '-' sign)
    foreach my $prm ( qw( -ip -hostname ) )
    {   
		my $attr = $prm;
		$attr =~ s/^-//;
		$attr = '_' . $attr;
		$self->_Set( $attr, $h_args{ $prm } );
	}
	
	# Try several authentication methods, the first working is OK
	$self->_AuthenticateFromSession() or $self->__AuthenticateFromIPorHostname(); 

	return $self;
}

=head2 Function __AuthenticateFromIPorHostname

 Title	  : __AuthenticateFromIPorHostname
 Usage	  : print "Authenticated" if ($self->__AuthenticateFromIPorHostname() == &TRUE);
 Prerequisite : The _ip or __hostname attributes exists and is NOT empty
 Function : Parse the hosts file checking if (ip | hostname) is correct.
            Start a new session and set the cookie
            If OK, set a lot of attributes
                
 Return	  : 1 => authentication is ok
 			0 => not authenticated (and no attribute changed)
 
=cut

sub __AuthenticateFromIPorHostname
{
	my $self = shift;
		
	my $ip = $self->_Get('_ip');
	my $hostname= $self->_Get('_hostname');
	

    
    $ip = '' if (! defined $ip);
    $hostname = '' if (! defined $hostname);

	if ($ip eq '' && $hostname eq '')
	{

		$self->_Trace("Starting __AuthenticateFromIPorHostname IP AND HOSTNAME EMPTY => FALSE\n");
		return &FALSE;
	}

	# Check password from file
	my $file_etc    = $self->_Get('__file');


	my $fh_file_etc = new IO::File($file_etc) or throw LipmError::IOException($file_etc, 'f');
	
	my $fnd_flg = &FALSE;
	my ($common_name,$role,$email,$workspace)   =  ('','','','');
    while (my $line = <$fh_file_etc>)
    {
        chomp($line);

        # blank lines are ignored
        next if (($line eq "") or ($line =~ /^#/)) ;

        my ($ip_host, @a_aliases) = split(/\s+/, $line);
		

        if ((($ip ne '') and ($ip_host eq $ip)) or (($hostname ne '') and (grep (/^$hostname$/, @a_aliases) ))) 
	    {
	        $fnd_flg     = &TRUE;
	        $common_name = $a_aliases[0];
			$role        = '';
			$email       = undef;
			$workspace   = undef;
            last;
		}
	};

	# If found, Start a session, and set the attributes if OK
	my @a_roles = split /;/,$role;
	if ( $fnd_flg == &TRUE && $self->_NewSession($ip,$common_name,\@a_roles,$workspace,$email) == &TRUE )
	{
		$self->_Trace("__AuthenticateFromIPorHostname: host $ip/$hostname authenticated\n");
		$self->_Set('_common_name',$common_name);
	    $self->_Set('_ra_roles',\@a_roles);
		$self->_Set('_workspace',$workspace);
        $self->_Set('_email',$email);
        $self->_Set('_Authenticated',&TRUE);
        return &TRUE;
	}
	else
	{
		$self->_Trace("__AuthenticateFromIPorHostname: host $ip/$hostname NOT authenticated\n");
		$self->_Set('_common_name','');
		$self->_Set('_ra_roles',[ 0 ]); 
		$self->_Set('_workspace','');
        $self->_Set('_email','');
        $self->_Set('_login','');
        $self->_Set('_Authenticated',&FALSE);
        $self->_Set('__password','');	# Do not keep useless password inmemory
		return &FALSE;
	}
}

1;
