
sub AdjustConfig
{
    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');

    my $rootdir = "$FindBin::RealBin/../";
    $rootdir = "$FindBin::RealBin/../../" if (!-e "$rootdir/site/cfg/site.cfg");

    $O_CONF->Update("$rootdir/site/cfg/site.cfg", 'O');

    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));

    $O_CONF->Update("$rootdir/site/cfg/site.cfg",           'O');
    $O_CONF->Update("$rootdir/site/cfg/authentication.cfg", 'O');

    $O_CONF->Set('admin_mail', $ENV{'MGB_ADMIN'}) if (defined $ENV{'MGB_ADMIN'});

    if (-e "$rootdir/data/cfg/local.cfg")
    {
        $O_CONF->Update("$rootdir/data/cfg/local.cfg", 'O');
    }

    if (-e "$rootdir/data/cfg/env.cfg")
    {
        $O_CONF->Update("$rootdir/data/cfg/env.cfg", 'O');
    }

    if ($O_CONF->Get('admin_mail') ne 'guest' && !IsEmailValid($O_CONF->Get('admin_mail')))
    {
        &Die('ERROR - admin_mail is not set - complete site/cfg/site.cfg file ', 'early');
    }

    if (!-e "$rootdir/cfg/local.cfg")
    {
        $O_CONF->Dump("$rootdir/data/cfg/local.cfg");
    }

    &Sanitize();

    if (!-e "$rootdir/data/cfg/ssmtp.conf")
    {
        &Die(
             'ERROR - ssmtp is not configured ['
               . $O_CONF->Get('portal_install_dir')
               . '/data/cfg/ssmtp.conf not found] - use '
               . $O_CONF->Get('portal_install_dir')
               . '/site/cfg/ssmtp.conf.template as a template',
             'early'
             );
    }

    $O_CONF->SetUnlessDefined('inchlib_clust_compress_threshold', 100);

    if (&OFFLINE_MODE == &TRUE)
    {
        $ENV{$O_CONF->Get('remote_user_env_name')} = $O_CONF->Get('admin_mail');
    }
}

sub __GetLocalUrl
{
    my $portal_web_server = 'localhost';
    my $portal_port       = 80;
    my $portal_http_root  = 'family-companion';

    $portal_web_server = $O_CONF->Get('portal_web_server') if ($O_CONF->IsDefined('portal_web_server'));
    $portal_port       = $O_CONF->Get('portal_port')       if ($O_CONF->IsDefined('portal_port'));

    if ($portal_port ne '80')
    {
        $portal_web_server .= ':' . $portal_port;
    }
    $portal_http_root = $O_CONF->Get('portal_http_root') if ($O_CONF->IsDefined('portal_http_root'));
    $portal_http_root =~ s/^\///;
    return "$portal_web_server/$portal_http_root";
}

sub __GetPublicUrl
{
    my $portal_web_server = 'localhost';
    my $portal_port       = 80;
    my $portal_http_root  = 'family-companion';

    $portal_web_server = $O_CONF->Get('portal_web_server') if ($O_CONF->IsDefined('portal_web_server'));
    $portal_port       = $O_CONF->Get('portal_port')       if ($O_CONF->IsDefined('portal_port'));

    if ($portal_port ne '80')
    {
        $portal_web_server .= ':' . $portal_port;
    }

    $portal_http_root = $O_CONF->Get('portal_http_root') if ($O_CONF->IsDefined('portal_http_root'));
    $portal_http_root =~ s/^\///;

    return "$portal_web_server/$portal_http_root";
}

sub __PrintConfirmationModal
{

    print '
<script>
$(function() {
	$(\'a[data-confirm]\').click(function(ev) {
		var href = $(this).attr(\'href\');
		
		if (!$(\'#dataConfirmModal\').length) {
			$(\'body\').append(\'<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">No</button><a class="btn btn-danger" id="dataConfirmOK">Yes</a></div></div></div></div>\');
		}
		$(\'#dataConfirmModal\').find(\'.modal-body\').text($(this).attr(\'data-confirm\'));
		$(\'#dataConfirmOK\').attr(\'href\', href);
		$(\'#dataConfirmModal\').modal({show:true});
		
		return false;
	});
});
</script>'

}

sub GetEmail
{
    my $userdir = shift;
    my $email   = '';
    if (-s "$userdir/contact.txt")
    {
        $email = `cat  $userdir/contact.txt`;
        chomp $email;
    }
    return $email;
}

sub SendMail
{
    my ($email, $subject, $body, $cc) = @_;

	#Mode local, je desactive le sendmail
	if ($O_CONF->Get('mode') eq 'local')
	{
		return 1;	
	}
	
    my @a_emails = split(',', $email);
    my @a_valid_emails = ();
    foreach my $email_test (@a_emails)
    {
        if (&IsEmailValid($email_test))
        {
            push(@a_valid_emails, $email_test);
        }
        else
        {
            print "ERROR - Email $email_test is not valid\n";
        }
    }

    if (scalar @a_valid_emails < 1)
    {
        croak("ERROR  - no valid email found in $email\n");
    }
    else
    {

        $email = join(',', @a_valid_emails);

        if (defined $cc)
        {
            my @a_ccs = split(',', $cc);
            my @a_valid_ccs = ();
            foreach my $cc_test (@a_ccs)
            {
                if (&IsEmailValid($cc_test))
                {
                    push(@a_valid_ccs, $cc_test);
                }
                else
                {
                    print "ERROR - Email $cc_test is not valid\n";
                }
            }
            if (scalar @a_valid_ccs < 1)
            {
                croak("ERROR - no valid email found in $cc\n");
            }
            else
            {
                $cc = join(',', @a_valid_ccs);
            }
        }

        #UTILISER la cmd sendmail

        #~ Subject: $subject
        #~ From: $cc | $admin_mail
        #~ To: $email
        #~ Cc: $cc if defined

        #~ $body
        #~ .

        my $from = $O_CONF->Get('admin_mail');
        $from = $cc if (defined $cc);

        my ($fh_mail, $mail_filename) = File::Temp::tempfile(UNLINK => 1, DIR => $O_CONF->Get('tmpdir'));
        print $fh_mail <<END;
Subject: $subject
From: $from
To: $email
END
        if (defined $cc)
        {
            print $fh_mail <<END;
Cc: $cc
END
        }

        print $fh_mail <<END;

$body
.
END
        $fh_mail->close();

        my $ssmtp_conf = "$FindBin::RealBin/../../data/cfg/ssmtp.conf";
        $ssmtp_conf = "$FindBin::RealBin/../data/cfg/ssmtp.conf" unless -e $ssmtp_conf;
        $ssmtp_conf = $ENV{'FC_DIR'} . '/data/cfg/ssmtp.conf'
          if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/ssmtp.conf');

        if (-e $ssmtp_conf)
        {
            my $mail_cmd = "sendmail -C $ssmtp_conf -t < $mail_filename";
            my $ret      = system $mail_cmd;
            if ($ret ne 0)
            {
                #die("ERROR - Send mail failed $mail_cmd ");
                print STDERR "ERROR - Send mail failed $mail_cmd ";
            }
            return 1;
        }
    }
    return 0;
}

sub IsEmailValid
{
    my $email = shift;

    if ($email !~ /^\S+\@\S+\.\S+/)
    {
        return 0;
    }
    else
    {
        return 1;
    }

}

sub Die
{
    my $msg       = shift;
    my $early     = shift;
    my $http_root = __GetLocalUrl();

    if (defined $early)
    {
        print <<END;
Content-type: text/html

<html lang="en"><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Family-Companion -Config Error</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="$http_root/web/css/portal.css">
    </head>
    <body>
END
    }

    print STDOUT "<div class='alert alert-danger'>$msg</div>";

    if (defined $early)
    {
        print <<END;
                </body></html>
END
    }
    exit 0;
}

sub __SetSessionStorage
{
    my ($portalname, $o_webparam) = @_;
    my $key   = $o_webparam->Get('key');
    my $owner = $o_webparam->Get('owner');

    return if (($key eq '') && ($owner eq ''));

    my $json = encode_json({'key' => $key, 'owner' => $owner});
    print <<END;
<script type="text/javascript">
if (! sessionStorage.$portalname) {
	sessionStorage.$portalname = '$json';
}
</script>
END
    return;
}

sub RunBuildQueryBuilder
{
    my $dir = shift;

    my $fh_lock = &GetStreamOut("$dir/db.lock");
    print $fh_lock time;
    $fh_lock->close();

    my $cmd = '('
      . $O_CONF->Get('portal_install_dir')
      . '/bin/int/create_family-companion_db.pl --dir='
      . $dir
      . " --lockfile=$dir/db.lock > $dir/db.build.out ) 2> $dir/db.build.err&";
    my ($status, $killed) = &RunExtNoWait(-cmd => $cmd, -snippet => 1);
    return ($cmd, $status, $killed);
}

=head2 procedure Die

 Title        : Kill
 Usage        : &Kill($message)
 Prerequisite : none
 Function     : Print an error message and exit with error code -1
 Returns      : none
 Args         : error message
 Globals      : none
	
=cut

sub Kill
{
    my $msg = shift;
    print STDERR "ERROR - $msg\n";
    &Exit(1);
}

sub Exit
{
    my $code = shift;
    unlink $OUTDIR . '/.pid';
    exit $code;
}

=head2 procedure System

 Title        : System
 Usage        : &System($cmd)
 Prerequisite : none
 Function     : Run a system command
 Returns      : none
 Args         : cmd to execute
 Globals      : none
	
=cut

sub System
{
    my $cmd = shift;
    &Log("CMD - $cmd");
    system($cmd);
    return;
}

=head2 procedure Log

 Title        : Log
 Usage        : &Log($msg)
 Prerequisite : none
 Function     : Print a message on stdout in --verbose or --debug mode
 Returns      : none
 Args         : message to print 
 Globals      : $VERBOSE
	
=cut

sub Log
{
    my $msg = shift;
    print STDOUT "$msg\n" if ($VERBOSE == &TRUE);
    return;
}

=head2 function Cat

 Title        : Cat
 Usage        : my $content = &Cat($file)
 Prerequisite : none
 Function     : Get any file content
 Returns      : File content
 Args         : File path
 Globals      : none
	
=cut

sub Cat
{
    my $file    = shift;
    my $content = '';
    my $fh      = &GetStreamIn($file);
    while (my $line = <$fh>)
    {
        $content .= $line;
    }
    $fh->close;
    return $content;
}

=head2 procedure Log

 Title        : Debug
 Usage        : &Debug($msg)
 Prerequisite : none
 Function     : Print a message on stdout in --debug mode
 Returns      : none
 Args         : message to print 
 Globals      : $DEBUG
	
=cut

sub Debug
{
    my $msg = shift;
    print STDOUT "$msg\n" if ($DEBUG == &TRUE);
    return;
}

sub __PrintFooter
{
    print STDOUT <<END;
<footer>
You are connected as $ENV{REMOTE_USER}. <br>
This site is maintained by: 
<address>
   <a href="mailto:lipm.bioinfo\@toulouse.inra.fr">LIPM Bioinfo Team</a>
</address>
</footer>
END
}

sub Sanitize
{
    my @a_to_strip = qw /portal_web_server portal_install_dir portal_http_root datahttpdir datarootdir /;

    foreach my $to_strip (@a_to_strip)
    {
        my $value = $O_CONF->Get($to_strip);
        $value =~ s,/$,,;
        $O_CONF->Set($to_strip, $value);
    }
}

sub __GenerateKey
{
    my $key = '';
    for (1 .. 8)
    {
        $key .= [0 .. 9, 'A' .. 'Z', 'a' .. 'z']->[rand 62];
    }
    return $key;
}

sub MinifyJson
{
    my ($infile, $outfile) = @_;
    $outfile = $infile unless (defined $outfile);
    &Kill("$infile does not exist") unless (-s $infile);

    my $data = decode_json(`cat $infile`);
    my $json = encode_json($data);

    my $fh_out = &GetStreamOut("$outfile");
    print $fh_out $json;
    $fh_out->close;
    return;
}
1;
