require 'common.pl';
require 'FamilyCompanion/Config.pl';
require 'FamilyCompanion/Utils.pl';
require 'FamilyCompanion/Parsers.pl';
require 'FamilyCompanion/SpecificAndOrthologousProteins.pl';
require 'FamilyCompanion/GroupAnalyses.pl';
require 'FamilyCompanion/CreatePanProteome.pl';
require 'FamilyCompanion/StatisticsAndMatrices.pl';
require 'FamilyCompanion/Venn.pl';

1;
