
=head2 function CreatePanProteome

 Title        : CreatePanProteome
 Usage        : &CreatePanProteome($rh_homologygroups,$rh_species,$rh_analysis_params,$o_param->Get('outdir'))
 Prerequisite : none
 Function     : Find for each proteome, the list of all group with a protein of this proteome + unique protein. Priority select seq from ref proteome then longer seq.
 Returns      : none
 Args         : hash ref of homologygroups groups, hash ref of species, hash ref of specific proteins, hash ref of analysis parameters,outdir
 Globals      : none

=cut

sub CreatePanProteome
{
    my ($rh_homologygroups, $rh_species, $rh_analysis_params, $rh_specific, $outdir) = @_;

    &Log("INFO - CreatePanProteome");

    my $refexist;
    my @a_references = ();

    my %h_list_group_select_seqid_code = ();
    my %h_nb_repre_group_by_proteome   = ();

    foreach my $code (keys %{$rh_analysis_params->{'proteomes'}})
    {
        if (   $rh_analysis_params->{'proteomes'}->{$code}->{'reference'} eq 'true'
            or $rh_analysis_params->{'proteomes'}->{$code}->{'reference'} == &TRUE)
        {
            &Debug("INFO - $code is a reference proteome");
            push(@a_references, $code);
        }
        $h_nb_repre_group_by_proteome{$code} = 0;
    }

	#paralelliser la recherche de sequence representative
	# utils.pl avec des objets storage rh_homologygroups rh_species en splitant le nombres de clés de rh_homologous et --start=trancheX

	my @a_groupids = sort (keys %{$rh_homologygroups});

	my $slice_size = int (scalar @a_groupids / $THREADS) + 1;
	my $fh_select_cmd = &GetStreamOut("$WORKDIR/cmd.selectrep");
	
	for (my $i = 0; $i < $THREADS; $i ++)
	{
		my $start = $i * $slice_size;
		print $fh_select_cmd "nice $FindBin::RealBin/bbric_family-companion-select_representative_prot.pl --workdir $WORKDIR --start $start --size $slice_size\n";
	}
	$fh_select_cmd->close();

	&System("(cat $WORKDIR/cmd.selectrep | parallel --tmpdir $WORKDIR  -j $THREADS > $WORKDIR/cmd.selectrep.out ) 2> $WORKDIR/cmd.selectrep.err");

	&Log("INFO - Merge Select representative slices");
	for (my $i = 0; $i < $THREADS; $i ++)
	{
		my $start = $i * $slice_size;
		my $rh_list_group_select_seqid_code_slice = retrieve ("$WORKDIR/h_list_group_select_seqid_code.$start");
		
		foreach my $k (keys %{$rh_list_group_select_seqid_code_slice})
		{
			$h_list_group_select_seqid_code{$k} = $rh_list_group_select_seqid_code_slice->{$k};
		}
	}

    &PrintPanProteome($outdir, $rh_specific, \%h_list_group_select_seqid_code,
                      $rh_species, \%h_nb_repre_group_by_proteome);



    return;
}


=head3 function PrintPanProteome

 Title        : PrintPanProteome
 Usage        : &PrintPanProteome($outdir,$rh_specific,\%h_list_group_select_seqid_code,$rh_species,$rh_nb_repre_group_by_proteome)
 Prerequisite : none
 Function     : Print pan-proteome and pan-proteome description
 Returns      : none
 Args         : outdir, hash of specific protein, hash of representative seqid/code for each homology group, hash of species, hash of number of representant group in pan proteome by species code
 Globals      : none

=cut

sub PrintPanProteome
{
    my ($outdir, $rh_specific, $rh_h_list_group_select_seqid_code, $rh_species, $rh_nb_repre_group_by_proteome) = @_;
    my $subdirectory             = "pan_proteome";
    my $subdirectory_description = "Pan proteome analysis";
    my $rh_description           = {$subdirectory => $subdirectory_description};
    mkdir "$outdir/$subdirectory";

    $rh_description->{"$subdirectory/PanProteome.fasta"} = 'Pan proteome multifasta file';
    my $fh_panproteome = &GetStreamOut("$outdir/$subdirectory/PanProteome.fasta");

    foreach my $group (sort keys %{$rh_h_list_group_select_seqid_code})
    {
        print $fh_panproteome ">"
          . $rh_h_list_group_select_seqid_code->{$group}[0] . "|"
          . $rh_h_list_group_select_seqid_code->{$group}[1] . "|"
          . $group . "\n";
        print $fh_panproteome $rh_species->{$rh_h_list_group_select_seqid_code->{$group}[1]}->{'sequences'}
          ->{$rh_h_list_group_select_seqid_code->{$group}[0]}->{'sequence'} . "\n";
        $rh_nb_repre_group_by_proteome->{$rh_h_list_group_select_seqid_code->{$group}[1]} =
          $rh_nb_repre_group_by_proteome->{$rh_h_list_group_select_seqid_code->{$group}[1]} + 1;
    }

    foreach my $code (keys %{$rh_specific})
    {
        foreach my $seqid (keys %{$rh_specific->{$code}})
        {
            print $fh_panproteome ">" . $seqid . "|" . $code . "|uniq\n";
            print $fh_panproteome $rh_species->{$code}->{'sequences'}->{$seqid}->{'sequence'} . "\n";
        }
    }
    $fh_panproteome->close;

    $rh_description->{"$subdirectory/PanProteomeDescription.xls"} = 'Pan proteome description table';
    my $fh_panproteome_desc = &GetStreamOut("$outdir/$subdirectory/PanProteomeDescription.xls");
    print $fh_panproteome_desc
      "#Species\tNb seq in panproteome\tNb seq rep group in panproteome\tNb seq uniq in panproteome\n";
    foreach my $code (keys %{$rh_species})
    {
        print $fh_panproteome_desc $code . "\t";
        print $fh_panproteome_desc $rh_nb_repre_group_by_proteome->{$code} + scalar(keys %{$rh_specific->{$code}})
          . "\t";
        print $fh_panproteome_desc $rh_nb_repre_group_by_proteome->{$code} . "\t";
        print $fh_panproteome_desc scalar(keys %{$rh_specific->{$code}}) . "\n";
    }
    $fh_panproteome_desc->close;

    my $fh_readme = &GetStreamOut("$outdir/$subdirectory/README");
    print $fh_readme <<END;
PanProteome.fasta contains a representative protein of each homology group and specific single copy proteins of all species

For each homology group
    If exist reference(s) species protein(s) in the group
        selection of the largest and high-quality protein in these proteins
    Else
        selection of the largest and high-quality protein of the group

For each species
    Add specific singlecopy protein
END
    $fh_readme->close;

    push(@A_OUTPUT_DATA, {'directory' => $subdirectory, 'description' => $rh_description});

    return;
}

1;

