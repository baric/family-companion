
=head2 procedure SetGlobals

 Title        : SetGlobals
 Usage        : SetGlobals()
 Prerequisite : none
 Function     : Set $DATE and $WORKDIR global variables
 Returns      : none
 Args         : none
 Globals      : none

=cut

sub SetGlobals
{
    
    $O_CONF = New ParamParser("$FindBin::RealBin/../../site/cfg/site.cfg");
    if (-e "$FindBin::RealBin/../../data/cfg/env.cfg")
    {
        $O_CONF->Update("$FindBin::RealBin/../../data/cfg/env.cfg", 'O');
    }

    
    $WORKDIR = tempdir(CLEANUP => 0, DIR => $O_CONF->Get('tmpdir'));
    $DATE = `date "+%Y%m%d %H:%M:%S"`;
    chomp $DATE;
    
    $MAFFT = &GetExePath('mafft');
    
    return;

}

=head2 function ValidAnalysisFile

 Title        : ValidAnalysisFile
 Usage        : my $rh_analysis_params = &ValidAnalysisFile($cfg_file)
 Prerequisite : none
 Function     : Valid the analysis configuration file (syntax and mandatory parameters)
 Returns      : Hash ref of analysis parameters
 Args         : JSON configuration file
 Globals      : none

=cut

sub ValidAnalysisFile
{
    my $cfg_file = shift;
    my $json_text = &Cat($cfg_file);
    
    my $rh_analysis_params = {};
    $rh_analysis_params = eval {from_json($json_text)};

    if ($@ ne '')
    {
        &Kill("Malformed analysis_cfg file : \n" . $@);
    }

    &__CheckMandatoryParameters($rh_analysis_params, dirname($cfg_file));
    &__SetOptionnalParameters($rh_analysis_params);

    return $rh_analysis_params;
}

=head2 procedure __SetOptionnalParameters

 Title        : __SetOptionnalParameters
 Usage        : &__SetOptionnalParameters($rh_analysis_params)
 Prerequisite : none
 Function     : Set default values to optionnal undefinde analysis parameters
 Returns      : none
 Args         : Hash ref of analysis parameters
 Globals      : $DATE

=cut

sub __SetOptionnalParameters
{
    my $rh_analysis_params = shift;
    $rh_analysis_params->{'title'} = 'Family analysis - ' . $DATE unless (defined $rh_analysis_params->{'title'});
    $rh_analysis_params->{'date'} = $DATE;

    delete $rh_analysis_params->{'homologygroups'}->{'outfile'} if (! -e  $rh_analysis_params->{'homologygroups'}->{'outfile'} or -z  $rh_analysis_params->{'homologygroups'}->{'outfile'});

	#$rh_analysis_params->{'homologygroups'}->{'method'} = 'orthomcl' unless defined ($rh_analysis_params->{'homologygroups'}->{'method'});
    $rh_analysis_params->{'homologygroups'}->{'method'} = 'orthofinder' unless defined ($rh_analysis_params->{'homologygroups'}->{'method'});

    if (defined $rh_analysis_params->{'homologygroups'}->{'outfile'} ) 
     {
        if (!defined $rh_analysis_params->{'homologygroups'}->{'version'})
        {
            my $homologygroups_outfile = $rh_analysis_params->{'homologygroups'}->{'outfile'};
            my $firstline        = `head -1 $homologygroups_outfile`;
            chomp $firstline;
            $rh_analysis_params->{'homologygroups'}->{'version'} = &__GuessHomologyGroupToolVersion($firstline);
        }
    }
    else
    {
        #$rh_analysis_params->{'homologygroups'}->{'version'} = '1.4 (blast+)';
        $rh_analysis_params->{'homologygroups'}->{'version'} = 'of.2.3.8';
    }

    &Log('INFO - Tool version ' . $rh_analysis_params->{'homologygroups'}->{'version'});
    foreach my $code (keys %{$rh_analysis_params->{'proteomes'}})
    {
        $rh_analysis_params->{'proteomes'}->{$code}->{'title'} = "$code proteome"
          if (!defined $rh_analysis_params->{'proteomes'}->{$code}->{'title'});
    }

    return;

}

=head2 function __GuessHomologyGroupToolVersion

 Title        : __GuessHomologyGroupToolVersion
 Usage        : my $version = &__GuessHomologyGroupToolVersion($firstline)
 Prerequisite : none
 Function     : Try to determine the OrthoMCL or other software version used to generate data
 Returns      : Major version (1.x or 2.x)
 Args         : First line of the homology group outfile
 Globals      : none

=cut

sub __GuessHomologyGroupToolVersion
{
    my $firstline = shift;
    my $version   = '2.x';
    #SC20150709:allVSallNoRepet1000(468 genes, 13 taxa): EPS32122(Penox1) estExt_fgenesh1_pg.C_4_t20369(Penla1)
    #if ($firstline =~ /^ORTHOMCL\d+\(\d+\s+genes,\d+\s+taxa\):\s+\S+\(\S+\)\s+\S+\(\S+\)/)
    if ($firstline =~ /^\w+\d+\(\d+\s+genes,\d+\s+taxa\):\s+\S+\(\S+\)\s+\S+\(\S+\)/)
    {
        $version = '1.x';
    }
    #Synergy output
    #Cluster205 (taxa: 2, genes: 2)  gene:NZ_AJJO01000256.1.10 gene:XaaCFBP6367064.10
    elsif ($firstline =~ /^Cluster\d+\s+\(taxa:\s+\d+,\s+genes:\s+\d+\)\s+gene:.*/)
    {
        $version = 'sy.x (Synergy output file)';
    }
    #OthoFinder output
    #OG002975: NC_002488.866 AWYH01000035.1.39 AXBS01000039.1.58
    elsif ($firstline =~ /^OG\d+:\s+.+/)
    {
        $version = 'of.x (OrthoFinder output file)';
    }
    return $version;
}

=head2 procedure  __CheckMandatoryParameters

 Title        : __CheckMandatoryParameters
 Usage        : &__CheckMandatoryParameters($rh_analysis_params, $cfg_dir)
 Prerequisite : none
 Function     : Check the validty of the mandatory parameters (file exists, number of files, set file path)
 Returns      : none
 Args         : Hash ref of analysis parameters, dirname to config file (to deal with relative path)
 Globals      : none

=cut

sub __CheckMandatoryParameters
{
    my ($rh_analysis_params, $cfg_dir) = @_;

    my @a_errors = ();


	if (! defined $rh_analysis_params->{'homologygroups'}->{'parameters'}  or ($rh_analysis_params->{'homologygroups'}->{'parameters'} eq ''))
	{
		my $parameters  = '';
		my @a_params = ('pi_cutoff','pv_cutoff','pmatch_cutoff','inflation');
		foreach my $param (@a_params)
		{
			$parameters .= " --$param=" . $rh_analysis_params->{'homologygroups'}->{$param}  if (defined $rh_analysis_params->{'homologygroups'}->{$param});
		}
		$rh_analysis_params->{'homologygroups'}->{'parameters'} = $parameters;
		
	}
    if (!defined $rh_analysis_params->{'contact'})
    {
        push(@a_errors, 'Contact email not defined');
    }
    else
    {
        push(@a_errors, 'Contact email ' . $rh_analysis_params->{'contact'} . 'is not a valid email address')
          if ($rh_analysis_params->{'contact'} !~ /\S+\@\S+\.\S+/);
    }

    if (!defined $rh_analysis_params->{'homologygroups'})
    {
        push(@a_errors, 'Homology groups data not defined');
    }

    if (   !defined $rh_analysis_params->{'homologygroups'}->{'outfile'}
        && !defined $rh_analysis_params->{'homologygroups'}->{'parameters'})
    {
        push(@a_errors, 'Homology groups parameters OR outfile not defined');
    }

    if (!defined $rh_analysis_params->{'proteomes'})
    {
        push(@a_errors, 'Proteome data not defined');
    }

    my $nb_proteomes = scalar keys %{$rh_analysis_params->{'proteomes'}};
    if ($nb_proteomes < 1)
    {
        push(@a_errors, "No proteome loaded");
    }

    my $iprscan_files = 0;
    foreach my $code (keys %{$rh_analysis_params->{'proteomes'}})
    {
        if (!defined $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'})
        {
            push(@a_errors, "Fasta file not defined for $code proteome");

        }
        else
        {
            if ($rh_analysis_params->{'proteomes'}->{$code}->{'fasta'} !~ /\//)
            {
                $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'} =
                  $cfg_dir . '/' . $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'};
            }

            if (!-e $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'})
            {

                push(@a_errors,
                         "Fasta file "
                       . $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'}
                       . " not found for $code proteome");
            }
        }

        if (defined $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} && ($rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} ne ''))
        {
            if ($rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} !~ /\//)
            {
                $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} =
                  $cfg_dir . '/' . $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'};
            }

            if (!-e $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'})
            {
                push(@a_errors, "IprScan provided file not found for $code proteome");
            }
            $iprscan_files++;
        }
    }

    if ($iprscan_files > 0 and $iprscan_files < $nb_proteomes)
    {
        push(@a_errors,
             "You must provide IprScan for ALL orf your proteomes or for NONE of them - found $iprscan_files/$nb_proteomes"
             );
    }

    if (scalar @a_errors > 0)
    {
        &Kill(join("\n", @a_errors));
    }
    return;
}

=head2 procedure  ConfigureWorkdir

 Title        : ConfigureWorkdir
 Usage        : &ConfigureWorkdir($rh_analysis_params)
 Prerequisite : none
 Function     : Prepare the working directory with all the files symlinked locally and overwrite analysis parameters with new path
 Returns      : none
 Args         : Hash ref of analysis parameters
 Globals      : $WORKDIR

=cut

sub ConfigureWorkdir
{
    my $rh_analysis_params = shift;
    foreach my $code (keys %{$rh_analysis_params->{'proteomes'}})
    {
        symlink(Cwd::abs_path($rh_analysis_params->{'proteomes'}->{$code}->{'fasta'}), "$WORKDIR/$code.fasta");
        $rh_analysis_params->{'proteomes'}->{$code}->{'fasta'} = "$WORKDIR/$code.fasta";

        if (defined $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} && -s $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'})
        {
            symlink(Cwd::abs_path($rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'}), "$WORKDIR/$code.iprscan");
            $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} = "$WORKDIR/$code.iprscan";
        }
        else
        {
			delete $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'} if (defined $rh_analysis_params->{'proteomes'}->{$code}->{'iprscan'});
		}
    }

    symlink(Cwd::abs_path($rh_analysis_params->{'homologygroups'}->{'outfile'}), "$WORKDIR/homologygroups.out") if (defined $rh_analysis_params->{'homologygroups'}->{'outfile'} && -s $rh_analysis_params->{'homologygroups'}->{'outfile'});

    return;

}

1;
