# BBRIC::Family-Companion #

<div class="alert alert-info">
Ludovic Cottret, Corinne Rancurel, Martial Briand, Sebastien Carrere
<footer><cite>BioArxiv</cite>, doi: <a target="_blank" href="https://doi.org/10.1101/266742">https://doi.org/10.1101/266742</a></footer>
</div>


<p><div class="toc">
<ul>
<li><a href="#family-companion">BBRIC::Family-Companion</a><ul>
<li><a href="#about">ABOUT</a></li>
<li><a href="#quickstart-docker">QUICKSTART DOCKER</a>
	<ul>
	
	    <li><a href="#prerequisites">PREREQUISITES</a></li>
		<li><a href="#build">BUILD</a></li>
		<li><a href="#hurry-want-to-run-locally-in-single-user-mode-">HURRY ? WANT TO RUN LOCALLY IN SINGLE USER MODE ?</a></li>
		<li><a href="#local-configuration">LOCAL CONFIGURATION</a></li>
		<li><a href="#standard-configuration">STANDARD CONFIGURATION</a></li>
		<li><a href="#create-admin-user">CREATE ADMIN USER</a></li>
		<li><a href="#run-app">RUN APP</a></li>
		<li><a href="#stop-app">STOP APP</a></li>
	</ul>
</li>

<li><a href="#expert-install">EXPERT INSTALL</a>
	<ul>
		<li><a href="#prerequisites">PREREQUISITES</a></li>
		<li><a href="#configuration">CONFIGURATION</a>
			<ul>
				<li><a href="#paths">PATHS</a></li>
				<li><a href="#admin-contact">ADMIN CONTACT</a></li>
				<li><a href="#authentication">AUTHENTICATION</a>
					<ul>
						<li><a href="#file">FILE</a></li>
						<li><a href="#ldap">LDAP</a></li>
						<li><a href="#shibboleth">SHIBBOLETH</a></li>
					</ul>
				</li>
				<li><a href="#smtp">SMTP</a></li>
			</ul>
		</li>
		<li><a href="#install">INSTALL</a></li>
		<li><a href="#apache">APACHE</a></li>
		<li><a href="#run">RUN</a></li>
	</ul>
</li>
<li><a href="#administration-tools">ADMINISTRATION TOOLS</a>
	<ul>
		<li><a href="#account-creation">ACCOUNT CREATION</a></li>
		<li><a href="#account-migration">ACCOUNT MIGRATION</a></li>
		<li><a href="#usage-report">USAGE REPORT</a></li>
	</ul>
</li>
<li><a href="#authors">AUTHORS</a></li>
<li><a href="#credits">CREDITS</a></li>
</ul>
</li>
</ul>
</div>
</p>


## ABOUT ##

Family-Companion, a web server dedicated to the computation, the analysis and the exploration of homology groups. </h1>

Family-Companion offers a user-friendly interface to launch and share private analyses, and interactive solutions to visualise proteome intersections Venn diagrams, phylogenetic trees, alignments and query (blast, search by keywords) the results .
Family-Companion allows biologists to easily perform and share proteome comparison  analyzes and provides them interpretation tools  to address the most common questions in this kind of approach. 
Family-Companion provides powerful, interactive and user-friendly web solutions to visualise and explore the inferred homology groups (alignments,  trees, phylogenetic profiles, Venn diagrams, summary charts).


## QUICKSTART DOCKER##

### PREREQUISITES ###

You need to install :

    docker-engine (see: https://docs.docker.com/engine/installation/)
    git

OPTIONNAL:
If you want to create users from host server (not from Docker image), you will need Perl and this perl module

    Apache::Htpasswd perl

### HURRY  ? WANT TO RUN LOCALLY IN SINGLE USER MODE ? ###

	curl -s https://framagit.org/BBRIC/family-companion/raw/master/local.sh | bash

:information_source: 
url to access the app: <b>http://localhost:1977/family-companion</b>
login/password to login into app: <b>guest/guest</b>
    
### BUILD ###

	sudo bash
	git clone https://framagit.org/BBRIC/family-companion.git
	cd family-companion
	export FC=$PWD
	$FC/build.sh

### LOCAL CONFIGURATION ###

Run Family-Companion locally:
	* login/password set to guest/guest
	* sendmail deactivated
	* persistence directory set to $HOME/.family-companion

	$FC/configure.sh --local

### STANDARD CONFIGURATION ###

#### Persistence directory ####
	
	export FC_DIR=/local/path/to/persistant/directory
	
#### Admin email adress ####
	
	export FC_ADMIN=john.doe@somwhere.org

#### App hostname and port ####
	
	export FC_HOSTNAME=myWebServer.org
	export FC_PORT=3232

#### SMTP Config ####
	
	export FC_SMTP=smtp.server.org:port

  optionnal

    export FC_SMTP_LOGIN=requiredlogin
    export FC_SMTP_PASSWD=requiredpasswd

:information_source: Exemple for GMAIL configuration (you will need to allow "Less secure apps" on your Gmail account (https://support.google.com/accounts/answer/6010255)):

	export FC_SMTP=smtp.gmail.com:587
	export FC_SMTP_SECURITY=STARTTLS
	export FC_SMTP_LOGIN=YourGmailLogin
	export FC_SMTP_PASSWD=YourGmailPassword
    
#### Run configuration shell script (will create dir/config files) ####
	
	$FC/configure.sh

	
### OPTIONNAL : CREATE ADMIN USER ###

:information_source: If you do not create admin user during installation process, these information will be asked at first web connexion

From the host server (need Apache::Htpasswd module):

	$FC/bin/int/admin_addUser.pl --email=$FC_ADMIN --password=<azekaze-aze> --common_name='Jon Doe' --outdir=$FC_DIR/etc

### RUN APP ###

:information_source: Run locally (guest/guest, no mail):

	$FC/run.sh --local

:information_source: To start Docker image in interactive mode run:

	$FC/run.sh --login


### STOP APP ###

	$FC/stop.sh


---

---

## EXPERT INSTALL ##

:information_source: Tested on Debian / Ubuntu > 14.04

### PREREQUISITES ###

You nee to install :

	Perl
	Apache 2.4
    git
    Apache::Htpasswd perl module

Then:

	cd /path/to/install/directory/
	git clone https://framagit.org/BBRIC/family-companion.git
	cd family-companion
	sudo bash
	export FC=$PWD

### CONFIGURATION ###

Configuration template file : `$FC/site/cfg/site.cfg.template`

	cp $FC/site/cfg/site.cfg.template $FC/site/cfg/site.cfg

#### PATHS ####

Set the following parameters in `$FC/site/cfg/site.cfg` configuration file:

    portal_install_dir=/file/system/root/access/to/family-companion (should be == to `$FC`)
    portal_http_root=/apache/root/access/to/family-companion
    portal_web_server=http://localhost

#### ADMIN CONTACT ####

Set the following parameter in `$FC/site/cfg/site.cfg` configuration file:

	admin_mail=contact@mail.org


#### AUTHENTICATION ####

Choose your authentication protocol by setting `authentication_mode` parameter in the configuration file `$FC/site/cfg/site.cfg` to one of these values:

- file
- ldap
- shibboleth

`file` is the default authentication mode used with Docker install script; it uses htaccess file authentication : simpliest way to use family-companion

Authentication template file : `$FC/site/cfg/authentication.cfg.template`

	cp $FC/site/cfg/authentication.cfg.template $FC/site/cfg/authentication.cfg

For each authentication mode, you have to set a list of parameters in `$FC/site/cfg/authentication.cfg` described below :

##### FILE #####

	authentic_file=PATH/TO/ETC/PASSWD/LIKE/FILE (default is `%I/etc/local.passwd`)
	authentic_htpasswd_file=PATH/TO/HTPASSWD/FILE (default is `%I/etc/local.htpasswd`)

:information_source: `%I` is a mask for `$FC`

##### LDAP #####

	authentic_ldap_host=LDAP DIRECTORY HOST NAME
	authentic_ldap_base=BASE DN
	authentic_ldap_port=LDAP DIRECTORY PORT

##### SHIBBOLETH #####

	authentic_sibboleth_login=LOGIN_URL/TO/Shibboleth.sso/Login
	authentic_sibboleth_logout=LOGOUT_URL/TO/Shibboleth.sso/Logout

:information_source: If you use a LDAP directory in complement to manage authorizations you have to set the following parameters

	authentic_ldap_host=LDAP DIRECTORY HOST NAME
	authentic_ldap_base=BASE DN
	authentic_ldap_port=LDAP DIRECTORY PORT

#### SMTP ####

Configure SMTP server using `$FC/site/cfg/ssmtp.conf.template` 
	
	mkdir -p $FC/data/cfg/
	chown -R www-data.www-data $FC/data
	cp $FC/site/cfg/ssmtp.conf.template $FC/data/cfg/ssmtp.conf 
	vi $FC/data/cfg/ssmtp.conf 

### INSTALL ###

Run install

	$FC/bin/int/admin_install.pl

:information_source: Follow dependencies installation instruction tha will be displayed in orange

### APACHE ###

Edit your Apache config file using `$FC/site/cfg/apache.conf` as a template to set correct rewrite directives
	
	vi /etc/apache2/sites-available/000-default.conf
	a2ensite 000-default

Restart Apache

	service apache2 reload

### RUN ###

Open a web browser to the right URL : http://`$portal_web_server`/`$portal_http_root`


## ADMINISTRATION TOOLS ##

### ACCOUNT CREATION ###

:information_source: This tool only works for `file` authentication mode (the one used with Docker install)

	$FC/bin/int/admin_addUser.pl --email=$USER_EMAIL --password=$USER_PASSWD --common_name='$USER_NAME' --outdir=$FC/etc

### ACCOUNT MIGRATION ###

:information_source: This tool can be used when one of your user change his mail address or when a PI wants to retrieve student analysis in his workspace

	$FC/bin/int/admin_migrate.pl --former_email=<Current.Case.Sensitive@email.address> --new_email=<New.Case.Sensitive@email.address> 

### USAGE REPORT ###

	sudo -u www-data $FC/bin/int/admin_stats.pl

produces a tabullated file with the following structure:

| key | email | date | error | dir_size | proteomes | iprscan | local_orthomcl | file |
| --------- | ---------- | ----------------- | ------------------- | ---------------------------- | ------------------- | ---------------------- | --------------------------- | --------------------------- |
| User hash | User email | analysis run date | erroneous (boolean) | complete analysis disk usage | number of proteomes | InterPro data uploaded | local execution of orthomcl | analysis configuration file |



## API ##

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/9e3b94e5f02d9b63bfe6)

## AUTHORS ##

* sebastien.carrere@toulouse.inra.fr
* ludovic.cottret@toulouse.inra.fr
* martial.briand@angers.inra.fr
* corinne.rancurel@sophia.inra.fr
* bbric.contact@toulouse.inra.fr  - [ <a href="https://bbric.toulouse.inra.fr">BBRIC Network</a> ]

## CREDITS ##

* O. Tange (2011): GNU Parallel - The Command-Line Power Tool, The USENIX Magazine, February 2011:42-47.

* B. Buchfink, Xie C., D. Huson, "Fast and sensitive protein alignment using DIAMOND", Nature Methods 12, 59-60 (2015).

