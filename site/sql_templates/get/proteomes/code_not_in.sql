SELECT
 DISTINCT #MASK_FIELD#
 FROM proteome
 WHERE
 upper(proteome.code) NOT IN ("#MASK_PROTEOME#")
 ORDER BY
 proteome.#MASK_FIELD# 
