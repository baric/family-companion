SELECT
 #MASK_FIELD#,
 homologyGroup.accession as homologyGroup,
 proteome.code as proteome
 FROM
 protein,
 homologyGroup,
 proteome
 WHERE
 homologyGroup.id = homologyGroup_id
 AND
 proteome.id = proteome_id
 AND
 protein.#MASK_FILTER# LIKE "#MASK_FILTER_VALUE#"
 #MASK_LIMIT# #MASK_OFFSET#
