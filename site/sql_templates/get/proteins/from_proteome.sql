SELECT #MASK_FIELD#,
 homologyGroup.accession as homologyGroup,
 proteome.code as proteome
 FROM
 protein,
 homologyGroup,
 proteome
 WHERE
 upper(proteome.code) IN ("#MASK_PROTEOME#")
 AND
 protein.homologyGroup_id = homologyGroup.id
 AND protein.proteome_id = proteome.id
 AND protein.#MASK_FILTER# LIKE "%#MASK_FILTER_VALUE#%"
 ORDER BY
 protein.proteome_id
 #MASK_LIMIT# #MASK_OFFSET#
