SELECT
 protein.id,
 proteome.code as proteome,
 protein.accession protein_accession,
 homologyGroup.accession as homologyGroup
 FROM
 homologyGroup,
 proteome,
 protein
 WHERE
 homologyGroup.id = protein.homologyGroup_id
 AND
 proteome.id = protein.proteome_id
 AND upper(proteome.code) IN ("#MASK_PROTEOME#")
 AND upper(homologyGroup.accession) IN ("#MASK_GROUP#")
