#!/usr/bin/perl

use strict;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int/";
use lib "$FindBin::RealBin/../../lib/ext/lipmutils";


use General;
use ParamParser;
use GeneralBioinfo;
use Runner;

use JSON;

#Pour cloner les $rh complexes
use Storable;
use File::Temp qw/ tempfile tempdir /;
use File::Basename;
use File::Copy;
use Cwd;

require 'FamilyCompanion.pl';

our $VERBOSE             = &FALSE;
our $DEBUG               = &FALSE;
our $ORTHOMCL            = "$FindBin::RealBin/../ext/ORTHOMCLV1.4-BBRIC/orthomcl.pl";
our $ORTHOFINDER_VERSION = '2.3.8';
our $ORTHOFINDER         = 'export PATH=$PATH:/var/www/.conda/envs/orthofinder/bin/; orthofinder';
our $TRIMAL              = "$FindBin::RealBin/../ext/trimal";

#Set dans SetGlobals
our $MAFFT;
our $DATE;
our $WORKDIR;

#our $ALIGNER = 'blast';
our $ALIGNER = 'diamond';

=head1 NAME

	bbric_family-companion.pl - Family companion tool

=head1 SYNOPSIS

	./bbric_family-companion.pl --outdir /home/michel/data --analysis_cfg /home/miche/myAnalysis.json 

=head1 DESCRIPTION

	A tool to produce homology groups supplementary files such as:
		- specific proteins
		- core proteomes
		- venn diagrams

	
=head1 CONFIGURATION

	{
	   "contact": "sebastien.carrere@toulouse.inra.fr",
	   "title" : "Low Stringency analysis with S. lycopersicum and V. vinifera",
	   "description" : "Orthomcl clustering analysis with 60 Percent Match Cutoff in ortholog clustering",
	   "homologygroups" : {
		  "method" : "orthomcl|orthofinder|synergy",
		  "parameters" : "pmatch_cutoff=60",
		  "version" : "1.4",
		  "outfile" : "/data/results/homologygroups.out"
	   },
	   "proteomes" : {
		  "solyc" : {
			 "reference" : true,
			 "fasta" : "/db/proteomes/solyc2.4.fasta",
			 "title" : "Solanum lycopersicum Heinz 1706 2.4 proteome",
			 "iprscan" : "/data/iprscan/data.ipr"
		  },
		  "helan" : {
			 "title" : "Helianthus annuus 412-HO 1.1 proteome",
			 "fasta" : "/db/proteomes/Ha412v1r1.fasta",
			 "iprscan" : "/data/iprscan/data.ipr",
			 "reference" : false
		  },
		  "arath" : {
			 "title" : "Arabidopsis thaliana Col-0 proteome",
			 "reference" : true,
			 "iprscan" : "/data/iprscan/data.ipr",
			 "fasta" : "/db/proteomes/tair10.fasta"
		  }
	   }
	}

=head1 SUBROUTINES

=cut

our @A_OUTPUT_DATA = ();
our $OUTDIR;
our $O_CONF;
our $THREADS = 4;

MAIN:
{
    my $o_param = New ParamParser('GETOPTLONG', \&Usage, 'verbose',  'debug', 'outdir=s', 'analysis_cfg=s');

    $VERBOSE = &TRUE if ($o_param->IsDefined('verbose'));
    if ($o_param->IsDefined('debug'))
    {
		$VERBOSE = &TRUE;
		$DEBUG = &TRUE ;
	}
	
    $o_param->AssertDirExists('outdir');
    $o_param->AssertFileExists('analysis_cfg');

    $OUTDIR = $o_param->Get('outdir');

    my $fh_pid = &GetStreamOut($OUTDIR . '/.pid');
    print $fh_pid $$;
    $fh_pid->close;

    &SetGlobals();
    $THREADS = $O_CONF->Get('threads') if ($O_CONF->IsDefined('threads'));

    my $rh_analysis_params = &ValidAnalysisFile($o_param->Get('analysis_cfg'));
    &ConfigureWorkdir($rh_analysis_params);

	my ($rh_species, $rh_seqidspeciescode) = &ParseProteomes($rh_analysis_params->{'proteomes'});

    if (!-e "$WORKDIR/homologygroups.out")
    {
        if ($rh_analysis_params->{'homologygroups'}->{'method'} eq 'orthofinder')
        {
            $rh_analysis_params->{'homologygroups'}->{'version'} = 'of.' . $ORTHOFINDER_VERSION;
            &RunOrthoFinder($rh_analysis_params);
        }
        else
        {
            $rh_analysis_params->{'homologygroups'}->{'version'} = '1.4 (blast+)';
            &RunOrthomcl($rh_analysis_params);
        }
    }

    
    my $rh_homologygroups = &ParseHomologyGroupsResults($rh_analysis_params->{'homologygroups'}->{'outfile'},
                                            $rh_analysis_params->{'homologygroups'}->{'version'},
                                            $rh_seqidspeciescode);

	store $rh_homologygroups, "$WORKDIR/rh_homologygroups";
	store $rh_species, "$WORKDIR/rh_species";
	store $rh_seqidspeciescode, "$WORKDIR/rh_seqidspeciescode";
	store $rh_analysis_params, "$WORKDIR/rh_analysis_params";

    my ($rh_count_proteins_in_group_by_code, $rh_count_inparalogs_by_code, $rh_count_specific_inparalogs_by_code) =
      &CheckProteomeCodesConsistency($rh_species, $rh_homologygroups);
    my $rh_specific_proteins = &GetSpecificProteins($rh_homologygroups, $rh_species);

    &GetAnalysisSummary($rh_species, $rh_count_proteins_in_group_by_code,
                        $rh_count_inparalogs_by_code, $rh_count_specific_inparalogs_by_code,
                        $rh_specific_proteins, $OUTDIR);

    &ExtractSpecificAndOrthologousProteins($rh_homologygroups, $rh_species, $rh_specific_proteins, $OUTDIR);
    &ListGroupAndUniqueForAllProteomes($rh_homologygroups, $rh_species, $rh_specific_proteins, $OUTDIR, $rh_analysis_params->{'contact'});
    &CreatePanProteome($rh_homologygroups, $rh_species, $rh_analysis_params, $rh_specific_proteins, $OUTDIR);
    &StatisticsAndMatrices($rh_homologygroups, $rh_analysis_params->{'proteomes'}, $OUTDIR);

    &CopyInputDataIntoOutdir($rh_analysis_params, $rh_species, $OUTDIR);

    &CreateGroupAnnotation($rh_homologygroups, $rh_species, $OUTDIR);

    &BuildSummaryFile($rh_analysis_params, $OUTDIR);

    &RunBuildQueryBuilder($OUTDIR);

    &Exit(0);

}

=head2 procedure  RunOrthomcl

 Title        : RunOrthomcl
 Usage        : &RunOrthomcl($rh_analysis_params)
 Prerequisite : none
 Function     : Run orthomcl.pl 1.4 (modified to use blast+)
 Returns      : none
 Args         : Hash ref of analysis parameters
 Globals      : $WORKDIR

=cut

sub RunOrthomcl
{
    my $rh_analysis_params = shift;

	RunBlast($rh_analysis_params);
    
    my $cmd =
        "( cd $WORKDIR; export BLAST_NOCPU=$THREADS; $ORTHOMCL"
      . ' --mode 3 --fa_files '
      . join('.fasta,', keys %{$rh_analysis_params->{'proteomes'}})
      . '.fasta --blast_file all.blast ';

    my $parameters = $rh_analysis_params->{'homologygroups'}->{'parameters'};
	$cmd .= " $parameters ";
	
    $cmd .= " >  $WORKDIR/run.orthomcl.stdout ) 2> $WORKDIR/run.orthomcl.stderr";
    &System($cmd);

    my $orthomcl_outfile = `ls $WORKDIR/*/all_orthomcl.out`;
    chomp $orthomcl_outfile;

    &Log("INFO - Copy $orthomcl_outfile to $WORKDIR/homologygroups.out");
    copy($orthomcl_outfile, "$WORKDIR/homologygroups.out");
    $rh_analysis_params->{'homologygroups'}->{'outfile'} = "$WORKDIR/homologygroups.out";

    my $tmpdir = `find $WORKDIR -maxdepth 1 -mindepth 1 -type d`;
    chomp $tmpdir;
    &Log("INFO - Clean workdir $tmpdir");

    if (($tmpdir ne '') && (-d $tmpdir) && ($tmpdir =~ /\/\w+_\d+$/))
    {
        &System("rm -rf $tmpdir");
    }
    return;
}

sub RunBlast
{
	my $rh_analysis_params = shift;

	
	my $cmd =
        "( cd $WORKDIR; cat " 
      . join('.fasta ', keys %{$rh_analysis_params->{'proteomes'}}) . '.fasta  | sed \'s/>/\n>/g;\' > all.fasta; dos2unix all.fasta;'
      . "$FindBin::RealBin/../ext/ncbi-blast/makeblastdb -in $WORKDIR/all.fasta -parse_seqids -dbtype prot 2>$WORKDIR/all.fasta.makeudb.log)"; 

	if ($ALIGNER eq 'diamond')
	{
		
		$cmd =
			"( cd $WORKDIR; cat " 
		. join('.fasta ', keys %{$rh_analysis_params->{'proteomes'}}) . '.fasta  | sed \'s/>/\n>/g;\' > all.fasta; dos2unix all.fasta;'
		. "$FindBin::RealBin/../ext/diamond makedb --in $WORKDIR/all.fasta --db  $WORKDIR/all.fasta.diamond  2>$WORKDIR/all.fasta.diamond.log)"; 

	}
	&System($cmd);
 
	my $parameters = $rh_analysis_params->{'homologygroups'}->{'parameters'};
	my ( $evalue ) = ($parameters =~ /pv_cutoff=(\S+)/);
	
	my $fh_cmd_blast = &GetStreamOut("$WORKDIR/cmd.blast");
	foreach my $proteome (keys %{$rh_analysis_params->{'proteomes'}})
    {
		#~ print $fh_cmd_blast "nice $FindBin::RealBin/../ext/usearch -ublast $WORKDIR/$proteome.fasta -db $WORKDIR/all.fasta.udb -evalue 1 -blast6out $WORKDIR/all.fasta.$proteome.blast\n";
		#~ print $fh_cmd_blast 
		my $cmd = "nice $FindBin::RealBin/../ext/ncbi-blast/blastp  -query $WORKDIR/$proteome.fasta -db $WORKDIR/all.fasta -evalue $evalue -out $WORKDIR/all.fasta.$proteome.blast -outfmt  '6 std' -num_threads 2 -max_target_seqs 1000";
		if ($ALIGNER eq 'diamond')
		{
			$cmd = "nice $FindBin::RealBin/../ext/diamond blastp --sensitive --query $WORKDIR/$proteome.fasta --db $WORKDIR/all.fasta.diamond --evalue $evalue --out $WORKDIR/all.fasta.$proteome.blast --threads 2 --max-target-seqs 1000";	
		}
		print $fh_cmd_blast "$cmd\n";
 	}
	$fh_cmd_blast->close();
	
    $cmd = "(cat $WORKDIR/cmd.blast | parallel --tmpdir $WORKDIR -j $THREADS > $WORKDIR/cmd.blast.out ) 2> $WORKDIR/cmd.blast.err";
    &System($cmd);


	$cmd = "( cd $WORKDIR; cat all.fasta.*.blast > all.blast)";
	&System($cmd);

	return;
}

=head2 procedure  RunOrthoFinder

 Title        : RunOrthoFinder
 Usage        : &RunOrthoFinder($rh_analysis_params)
 Prerequisite : none
 Function     : Run Ortho Finder 0.2.8
 Returns      : none
 Args         : Hash ref of analysis parameters
 Globals      : $WORKDIR

=cut

sub RunOrthoFinder
{
    my $rh_analysis_params = shift;
    my $cmd                = "( cd $WORKDIR;$ORTHOFINDER -f $WORKDIR";
    $cmd .= " >  $WORKDIR/run.orthofinder.stdout ) 2> $WORKDIR/run.orthofinder.stderr";
    &System($cmd);

    my $orthofinder_outfile = `ls $WORKDIR/OrthoFinder/Results_*/Orthogroups/Orthogroups.txt`;
    chomp $orthofinder_outfile;

    &Log("INFO - Copy $orthofinder_outfile to $WORKDIR/homologygroups.out");
    copy($orthofinder_outfile, "$WORKDIR/homologygroups.out");
    $rh_analysis_params->{'homologygroups'}->{'outfile'} = "$WORKDIR/homologygroups.out";

    return;
}

=head2 procedure Usage

 Title        : Usage
 Usage        : &Usage()
 Prerequisite : none
 Function     : Program Usage
 Returns      : none
 Args         : none
 Globals      : none
	
=cut

sub Usage
{
    print STDOUT <<END;
	$0
	 --outdir 			Output directory (where the files will be created)
	 --analysis_cfg		Analysis configuration file (JSON formatted)
	 
	 Optionnal:
	 --verbose
END
    return;
}

