#!/usr/bin/perl

use strict;
use FindBin;
use lib "$FindBin::RealBin/../../lib/int/";
use lib "$FindBin::RealBin/../../lib/ext/lipmutils";

use General;
use ParamParser;
use GeneralBioinfo;

use JSON;

#Pour cloner les $rh complexes
use Storable;
use File::Temp qw/ tempfile tempdir /;
use File::Basename;
use File::Copy;
use Cwd;

require 'common.pl';

our $O_CONF;

MAIN:
{
    my $user = $ENV{'USER'};
    if (!defined $ENV{'USER'} && defined $ENV{'SERVER_SOFTWARE'})
    {
        $user = 'www-data';
    }

    if ($user ne 'www-data')
    {
        die("ERROR - Must be run as  www-data user\n");
    }

    $O_CONF = New ParamParser('INIT');
    $O_CONF->SetBehaviour('use_substitution_table');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg", 'O');
    $O_CONF->SetSubstitution('%S', \$O_CONF->Get('portal_web_server'));
    $O_CONF->SetSubstitution('%I', \$O_CONF->Get('portal_install_dir'));
    $O_CONF->SetSubstitution('%H', \$O_CONF->Get('portal_http_root'));
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/site.cfg",           'O');
    $O_CONF->Update("$FindBin::RealBin/../../site/cfg/authentication.cfg", 'O');

    my $local_cfg = undef;
    $local_cfg = "$FindBin::RealBin/../../data/cfg/local.cfg" if (-e "$FindBin::RealBin/../../data/cfg/local.cfg");
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/local.cfg' if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/local.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";
    $local_cfg = $ENV{'FC_DIR'} . '/data/cfg/env.cfg'  if (defined $ENV{'FC_DIR'} && -e $ENV{'FC_DIR'} . '/data/cfg/env.cfg');
    $O_CONF->Update($local_cfg, 'O') if defined "$local_cfg";

	&Sanitize();

    
    my $datarootdir = $O_CONF->Get('datarootdir');

    my $cmd              = "find $datarootdir -name 'analysis.json'";
    my @a_analysis_files = `$cmd`;
    chomp @a_analysis_files;

    print STDOUT "#key\temail\tdate\terror\tdir_size\tproteomes\tiprscan\tlocal_orthomcl\tfile\n";
    foreach my $file (@a_analysis_files)
    {
        $file = Cwd::abs_path($file);
        my $dir      = dirname($file);
        my $dir_size = `du -sh $dir | cut -f1`;
        chomp $dir_size;

        my $key  = basename(dirname($dir));
        my $json = `cat $file`;

        my $error = &FALSE;
        if (-s "$dir/error")
        {
            $error = &TRUE;
        }
        chomp $json;

        my $rh_analysis = from_json($json);

        my $email = $rh_analysis->{'contact'};
        my $date  = $rh_analysis->{'date'};

        my @a_proteomes = keys %{$rh_analysis->{'proteomes'}};
        my $iprscan     = &FALSE;

        my $proteomes = scalar(@a_proteomes);
        if ($rh_analysis->{'proteomes'}->{$a_proteomes[0]}->{'iprscan'} ne '')
        {
            $iprscan = &TRUE;
        }

        my $local_orthomcl = &TRUE;

        if ($rh_analysis->{'orthomcl'}->{'outfile'} ne '')
        {
            $local_orthomcl = &FALSE;
        }

        print STDOUT "$key\t$email\t$date\t$error\t$dir_size\t$proteomes\t$iprscan\t$local_orthomcl\t$file\n";

    }

}

